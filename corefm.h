/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QWidget>

#include <cprime/systemxdg.h>

#include "global.h"

class QMenu;
class QListWidgetItem;
class FolderView;
class StorageManager;

namespace Ui {
class corefm;
}

class corefm : public QWidget
{
	Q_OBJECT

public:
	explicit corefm(QString path = QDir::homePath(), QWidget *parent = nullptr);
	~corefm();

protected:
	void closeEvent(QCloseEvent *event) override;

private slots:
	void on_tools_clicked();

private:
	Ui::corefm *ui;
	bool showHidden, showThumb, isSplit, disableTrashConfirmationMessage, windowMaximized;
	int uiMode, viewMode, sortMode;
	QSize toolsIconSize, listViewIconSize, iconViewIconSize, windowSize;

	/* Toolbar Actions */
	QAction *newFolderAct, *newFileAct, *showHiddenAct, *previewAct, *termAct;
	/* Toolbar and menu actions */
	QAction *cutAct, *copyAct, *pasteAct;
	/* Menu actions */
	QAction *openAct, *refreshAct, *selectAllAct, *renameAct, *trashAct, *deleteAct, *propsAct;
	QAction *batchRenameAct, *newWindowAct, *openWithAct;
	/* Other actions */
	QAction *backAct, *upAct, *splitAct, *openInSplitAct, *viewChangeAct, *treeWriteAct, *metadataAct, *searchAct, *sortChangeAct;
	QAction *zoomInAct, *zoomOutAct;

	/* FileSystemViews */
	FolderView *view1, *view2;

	/* Move across the windows */
	static bool moveItems;

	QClipboard *clipBoard;
	CPrime::SystemXdgMime *mimeH;

	void loadSettings();
	void startSetup();
	void showSideView();
	void setupIcons();

	void setupActions();
	void makeConnections();

	/* Current View */
	FolderView * currentView();

	QMenu * openWithMenu();

	StorageManager *sMgr;
	settings *smi;


private Q_SLOTS:
	/* Create a new folder in the current view */
	void newFolder();
	void newFile();
	void toggleHidden();
	void togglePreviews();
	void openTerminal();
	void prepareMove();
	void prepareCopy();
	void paste();
	void refresh();
	void selectAll();
	void rename();
	void trashSelected();
	void deleteSelected();
	void showProperties();
	void goBack();
	void goForward();
	void goUp();
	void writeTree();
	void openSearch();
	void updateVarious();
	void newWindow(QString path = QDir::homePath());
	void openInSplitView();
	void loadAddress(QString path = QDir::homePath());
	void openInApp(QString path   = QString());
	void selectApp();
	void contextMenu(const QPoint&);
	void toggleSplit();
	void nextView();
	void nextSortMode();
	void openMetadataViewer();
	void openRenamer();
	void on_shareIt_clicked();
	void on_pinIt_clicked();
	void on_trash_clicked();
	void on_home_clicked();
	void on_desktop_clicked();
	void on_downloads_clicked();
	void on_root_clicked();
	void on_drives_clicked();
	void loadDrivePage();
	void openSelecteDrive(QListWidgetItem *item);
};
