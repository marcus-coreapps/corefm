/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/


#include "filesystemview.hpp"

#include <QResizeEvent>
#include <QHeaderView>

#include <cprime/ioprocesses.h>
#include <cprime/filefunc.h>

/* ##### ItemDelegate ========================================================================================================== */

/*
  *
  * This code is contributed by eyllanesc (https://stackoverflow.com/users/6622587/eyllanesc)
  * In Qt, sometimes there are issues with text eliding in QListView::ListMode. When using
  * uniformItemSizes, the item does not utilize the entire space available due to the grid.
  * Setting the correct sizeHint based on the gridSize, ensures that complete space is used.
  *
  */
QSize IconDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	QSize size(QStyledItemDelegate::sizeHint(option, index));

	/* Icon View */
	if (option.decorationPosition == QStyleOptionViewItem::Top)
	{
		size.setWidth(static_cast<int>(FileSystemView::gridWidth) - 5);
		return size;
	}

	/* Tile View */
	else if (option.decorationPosition == QStyleOptionViewItem::Left)
	{
		size.setWidth(static_cast<int>(FileSystemView::gridWidth) - 5);
		return size;
	}

	return size;
}


/* ##### FileSystemView ========================================================================================================== */
quint64 FileSystemView::gridWidth = 256;

FileSystemView::FileSystemView(QWidget *parent)
	: QListView(parent)
	, smi(new settings)
{
	// qWarning() << "0";

	setFrameStyle(QFrame::NoFrame);
	setSelectionBehavior(QAbstractItemView::SelectRows);

	// qWarning() << "1";

	QSize lsize = smi->getValue("CoreApps", "IconViewIconSize");
	int   iSize = lsize.width();
	/* Ensure the smallest icon size is 16px, otherwise make it 48px */
	iSize = (iSize < 16 ? 48 : iSize);
	/* Make sure icon size is a multiple of 8 */
	iSize = (iSize % 8 ? (static_cast<int>(iSize / 8) + 1) * 8 : iSize);
	setIconSize(QSize(iSize, iSize));

	int viewMode = smi->getValue("CoreFM", "ViewMode");
	if (viewMode == 1)
	{
		setTileView();
	}

	else
	{
		setIconView();
	}

	// qWarning() << "2";

	setResizeMode(QListView::Adjust);
	setMovement(QListView::Static);
	setSelectionMode(QListView::ExtendedSelection);
	setFlow(QListView::LeftToRight);
	setWrapping(true);
	setWordWrap(false);
	setTextElideMode(Qt::ElideRight);
	setUniformItemSizes(true);

	// qWarning() << "3";

	setContextMenuPolicy(Qt::CustomContextMenu);

	setDragDropMode(QAbstractItemView::NoDragDrop);
	setDragEnabled(false);
	setAcceptDrops(false);

	// qWarning() << "4";

	// setMinimumSize( QSize( 270, 400 ) );

	setItemDelegate(new IconDelegate());

	QPalette pltt(palette());
	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);

	// qWarning() << "5";
}


FileSystemView::~FileSystemView()
{
	delete smi;
}


void FileSystemView::setIconView()
{
	setViewMode(QListView::IconMode);

	/* Recompute the gridSize */
	setIconSize(iconSize());
}


void FileSystemView::setTileView()
{
	setViewMode(QListView::ListMode);

	/* Recompute the gridSize */
	setIconSize(iconSize());
}


void FileSystemView::wheelEvent(QWheelEvent *event)
{
	QPoint wheelScroll = event->angleDelta();

	if (event->modifiers() & Qt::ControlModifier)
	{
		QSize newSize = iconSize();
		bool  increment;

		if ((wheelScroll.x() > 0) || (wheelScroll.y() > 0))
		{
			newSize  += QSize(8, 8);
			increment = true;
		}
		else
		{
			newSize  -= QSize(8, 8);
			increment = false;
		}

		if ((newSize.width() > 96) || (newSize.width() < 16))
		{
			return;
		}

		// 16 24 32 \40/ 48 \56/ 64 \72/ \80/ 88 96
		int badSize[] = { 40, 56, 72, 80, 72, 56, 40 };
		for (int i = 0; i < 7; i++)
		{
			if (newSize.width() == badSize[i])
			{
				if (increment)
				{
					newSize += QSize(8, 8);
				}
				else
				{
					newSize -= QSize(8, 8);
				}
			}
		}

		setIconSize(newSize);

		return;
	}

	QListView::wheelEvent(event);
}


void FileSystemView::setModel(QAbstractItemModel *fsm)
{
	cModel = qobject_cast<FileSystemModel *>(fsm);

	QListView::setModel(fsm);
	connect(fsm, SIGNAL(updateItem(QModelIndex)), this, SLOT(updateItem(QModelIndex)));
}


void FileSystemView::setIconSize(const QSize& size)
{
	QListView::setIconSize(size);

	/* Icon View */
	int viewMode = smi->getValue("CoreFM", "ViewMode");
	if (viewMode == 0)
	{
		int minWidth = ( int )qMin(256, qMax(64, static_cast<int>(size.width() * 1.6)));
		int height   = qMax(size.height() + 21, size.height() * 2);

		/* Items per row */
		int items = ( int )(viewportWidth / (minWidth + spacing()));

		/* Minimum width of all items */
		int itemsWidth = items * minWidth;

		/* Empty space remaining */
		int empty = viewportWidth - itemsWidth;

		/* Extra space per item */
		int extra = (empty / items) - 5;

		/* New grid size */
		QListView::setGridSize(QSize(minWidth + extra, height));

		gridWidth = static_cast<quint64>(minWidth + extra);
	}

	/* Tile View */
	else
	{
		QListView::setGridSize(QSize(256, size.height() + 16));
		gridWidth = 256;
	}
	smi->setValue("CoreApps", "IconViewIconSize", size);
}


void FileSystemView::setGridSize(const QSize&)
{
	setIconSize(iconSize());
}


void FileSystemView::resizeEvent(QResizeEvent *rEvent)
{
	rEvent->accept();

	/* To prevent crashes while hiding the split view */
	viewportWidth = qMax(viewport()->width(), 128);
	setIconSize(iconSize());
}


void FileSystemView::updateItem(QModelIndex idx)
{
	QAbstractItemView::dataChanged(idx, idx, QVector<int>() << Qt::DecorationRole);
}


void FileSystemView::copy()
{
	CPrime::IOProcess *process = new CPrime::IOProcess;

	process->sourceDir = CPrime::FileUtils::dirName(ioArgs.at(0));
	process->targetDir = ioArgs.takeLast();

	if (not process->sourceDir.endsWith("/"))
	{
		process->sourceDir += "/";
	}

	QStringList srcList;

	Q_FOREACH ( QString file, ioArgs )
	{
		if (CPrime::FileUtils::exists(file))
		{
			srcList << file.replace(process->sourceDir, "");
		}
	}

	if (not srcList.count())
	{
		return;
	}

	process->type = CPrime::Copy;

	IODialog *pasteDlg = new IODialog(srcList, process);

	pasteDlg->show();

	ioArgs.clear();
}


void FileSystemView::move()
{
	CPrime::IOProcess *process = new CPrime::IOProcess;

	process->sourceDir = CPrime::FileUtils::dirName(ioArgs.at(0));
	process->targetDir = ioArgs.takeLast();

	if (not process->sourceDir.endsWith("/"))
	{
		process->sourceDir += "/";
	}

	QStringList srcList;

	Q_FOREACH ( QString file, ioArgs )
	{
		if (CPrime::FileUtils::exists(file))
		{
			srcList << file.replace(process->sourceDir, "");
		}
	}

	if (not srcList.count())
	{
		return;
	}

	process->type = CPrime::Move;

	IODialog *pasteDlg = new IODialog(srcList, process);

	pasteDlg->show();

	ioArgs.clear();
}


void FileSystemView::link()
{
	QString path = ioArgs.takeLast();

	Q_FOREACH ( QString node, ioArgs )
	{
		QFile::link(node, QDir(path).filePath(CPrime::FileUtils::baseName(node)));
	}

	ioArgs.clear();
}


/* ##### DetailsSystemView ========================================================================================================== */
DetailsSystemView::DetailsSystemView(QWidget *parent) : QTableView(parent)
	, smi(new settings)
{
	setFrameStyle(QFrame::NoFrame);

	setGridStyle(Qt::NoPen);
	setSelectionBehavior(QAbstractItemView::SelectRows);

	QSize lsize = smi->getValue("CoreApps", "ListViewIconSize");
	int   iSize = lsize.width();

	/* Ensure the smallest icon size is 16px, otherwise make it 48px */
	iSize = (iSize < 16 ? 48 : iSize);
	/* Make sure icon size is a multiple of 8 */
	iSize = (iSize % 8 ? (static_cast<int>(iSize / 8) + 1) * 8 : iSize);
	setIconSize(QSize(iSize, iSize));

	verticalHeader()->hide();
	horizontalHeader()->setMinimumSectionSize(150);

	setContextMenuPolicy(Qt::CustomContextMenu);

	setDragDropMode(QAbstractItemView::NoDragDrop);
	setDragEnabled(false);
	setAcceptDrops(false);

	QPalette pltt(palette());
	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);
}


DetailsSystemView::~DetailsSystemView()
{
	delete smi;
}


void DetailsSystemView::setModel(QAbstractItemModel *fsm)
{
	cModel = qobject_cast<FileSystemModel *>(fsm);

	QTableView::setModel(fsm);

	horizontalHeader()->setStretchLastSection(false);
	horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);

	connect(fsm, SIGNAL(updateItem(QModelIndex)), this, SLOT(updateItem(QModelIndex)));
}


void DetailsSystemView::setIconSize(const QSize& size)
{
	QTableView::setIconSize(size);
	verticalHeader()->setDefaultSectionSize(size.height() + 8);
	smi->setValue("CoreApps", "ListViewIconSize", size);
}


void DetailsSystemView::updateItem(QModelIndex idx)
{
	QAbstractItemView::dataChanged(idx, idx, QVector<int>() << Qt::DecorationRole);
}


void DetailsSystemView::wheelEvent(QWheelEvent *event)
{
	QPoint wheelScroll = event->angleDelta();

	if (event->modifiers() & Qt::ControlModifier)
	{
		QSize newSize = iconSize();
		bool  increment;

		if ((wheelScroll.x() > 0) || (wheelScroll.y() > 0))
		{
			newSize  += QSize(8, 8);
			increment = true;
		}
		else
		{
			newSize  -= QSize(8, 8);
			increment = false;
		}

		if ((newSize.width() > 96) || (newSize.width() < 16))
		{
			return;
		}

		// 16 24 32 \40/ 48 \56/ 64 \72/ \80/ 88 96
		int badSize[] = { 40, 56, 72, 80, 72, 56, 40 };
		for (int i = 0; i < 7; i++)
		{
			if (newSize.width() == badSize[i])
			{
				if (increment)
				{
					newSize += QSize(8, 8);
				}
				else
				{
					newSize -= QSize(8, 8);
				}
			}
		}

		setIconSize(newSize);

		return;
	}

	QTableView::wheelEvent(event);
}
