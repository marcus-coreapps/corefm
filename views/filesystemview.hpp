/*
  *
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  *
  */


#pragma once

#include <QStyledItemDelegate>
#include <QListView>
#include <QTableView>

#include "global.h"
#include "filesystemmodel.h"


class IconDelegate : public QStyledItemDelegate {
	Q_OBJECT

public:
	QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

class FileSystemView : public QListView {
	Q_OBJECT

public:
	FileSystemView(QWidget *parent);
	~FileSystemView();

	void setModel(QAbstractItemModel *) Q_DECL_OVERRIDE;

	void setIconSize(const QSize&);
	void setGridSize(const QSize&);             // Suitable GridSize automatically set

	void setIconView();
	void setTileView();

	static quint64 gridWidth;

private:
	int viewportWidth = 512;
	FileSystemModel *cModel;
	settings *smi;

	QStringList ioArgs;

	// QPoint dragStartPosition;

protected:
	void wheelEvent(QWheelEvent *event) override;
	void resizeEvent(QResizeEvent *rEvent);

	void mousePressEvent(QMouseEvent *mEvent)
	{
		emit gotFocus();

		QListView::mousePressEvent(mEvent);
	}

public Q_SLOTS:
	void updateItem(QModelIndex idx);

private Q_SLOTS:
	void copy();
	void move();
	void link();

Q_SIGNALS:
	void gotFocus();
};

class DetailsSystemView : public QTableView {
	Q_OBJECT

public:
	DetailsSystemView(QWidget *parent);
	~DetailsSystemView();

	void setModel(QAbstractItemModel *) Q_DECL_OVERRIDE;

	void setIconSize(const QSize&);

private:
	FileSystemModel *cModel;
	settings *smi;

public Q_SLOTS:
	void updateItem(QModelIndex idx);

protected:
	void wheelEvent(QWheelEvent *event) override;

	void mousePressEvent(QMouseEvent *mEvent)
	{
		emit gotFocus();

		QTableView::mousePressEvent(mEvent);
	}

Q_SIGNALS:
	void gotFocus();
};
