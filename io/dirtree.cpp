/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/


#include "dirtree.h"

#include <QFileInfo>
#include <QDebug>
#include <dirent.h>

TreeWriter::TreeWriter(QObject *parent)
	: QThread(parent)
{
}


void TreeWriter::writeTree(QString path, QString filename)
{
	tree = new QFile(filename);
	if (not tree->open(QFile::WriteOnly))
	{
		qDebug() << "Unable to open file for writing";
		return;
	}

	mPath = path;
	start();
}


void TreeWriter::run()
{
	getTree(mPath, " \xe2\x94\x82");
	tree->close();
	qDebug() << "Writing the tree complete.";
}


void TreeWriter::getTree(QString dir, QByteArray indent)
{
	QString       filepath;
	DIR           *dp;
	struct dirent *dirp;

	dp = opendir(dir.toUtf8().constData());

	if (dp == NULL)
	{
		// cout << "Error " << errno << " while trying opening" << dir << endl;
		return;
	}

	/* Read all files and folders in current directory */
	while ((dirp = readdir(dp)))
	{
		/* Setup file path string, i.e. "Git/cpp/folder1/file.txt" */
		filepath = dir + "/" + dirp->d_name;

		/* Excludes: Ignore . .. .git .svn */
		if (strcmp(dirp->d_name, ".") == 0)
		{
			continue;
		}

		else if (strcmp(dirp->d_name, "..") == 0)
		{
			continue;
		}

		else if (strcmp(dirp->d_name, ".git") == 0)
		{
			continue;
		}

		else if (strcmp(dirp->d_name, ".svn") == 0)
		{
			continue;
		}

		tree->write(indent.left(indent.size() - 3) + "\xe2\x94\x9c\xe2\x94\x80 " + QByteArray(dirp->d_name) + "\n");

		/* recurse into the current node if its a directory */
		if (dirp->d_type == DT_DIR)
		{
			getTree(filepath, indent + "   \xe2\x94\x82");
		}

		else if ((dirp->d_type == DT_UNKNOWN) and QFileInfo(filepath).isDir())
		{
			getTree(filepath, indent + "   \xe2\x94\x82");
		}
	}

	closedir(dp);
}
