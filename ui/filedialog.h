/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QDialog>
#include <QDir>

namespace Ui {
class filedialog;
}

class filedialog : public QDialog
{
	Q_OBJECT

public:
	enum Mode
	{
		NewFolder = 0x21A7F9,
		NewFile,
		Rename
	};

	explicit filedialog(filedialog::Mode mode, QDir dir, QString text = QString(), QWidget *parent = nullptr);
	~filedialog();

	QString getCurrentText() const;
	QString name();

private:
	void setWindowProperties();

	QDir dir;
	QString data;
	bool useSystemNotification;

	filedialog::Mode dlgMode;

public Q_SLOTS:
	void accept();

private slots:
	void cancel();
	void handleTextChanged(QString newText);

private:
	Ui::filedialog *ui;

	void shotcuts();
};
