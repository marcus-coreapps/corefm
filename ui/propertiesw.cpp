/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include "propertiesw.h"
#include "ui_propertiesw.h"

#include <QDirIterator>
#include <QStorageInfo>
#include <QScreen>
#include <QPainter>
#include <QValidator>

#include <sys/stat.h>

#include <cprime/filefunc.h>

void SizeRunner::run()
{
	files     = 0;
	folders   = 0;
	totalSize = 0;

	Q_FOREACH ( QString path, mPaths )
	{
		if (mTerminate)
		{
			emit finalCount(files, folders, totalSize);
			return;
		}

		if (CPrime::FileUtils::isDir(path))
		{
			recurseProperties(path);
		}

		else
		{
			files++;
			totalSize += fileSize(path);
		}
	}

	emit finalCount(files, folders, totalSize);
	mTerminate = false;
}


quint64 SizeRunner::fileSize(QString path)
{
	struct stat statbuf;

	if (stat(path.toLocal8Bit().constData(), &statbuf) != 0)
	{
		return 0;
	}

	return statbuf.st_size;
}


void SizeRunner::recurseProperties(QString path)
{
	QDirIterator it(path, QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::NoSymLinks | QDir::Hidden, QDirIterator::Subdirectories);

	while (it.hasNext())
	{
		if (mTerminate)
		{
			return;
		}

		it.next();
		if (it.fileInfo().isDir())
		{
			if (it.filePath() == path)
			{
				continue;
			}

			folders++;
		}

		else
		{
			files++;
			totalSize += it.fileInfo().size();
		}

		if ((folders + files) % 1000 == 0)
		{
			emit currentCount(files, folders, totalSize);
		}
	}

	emit currentCount(files, folders, totalSize);
}


propertiesw::propertiesw(const QString paths, QWidget *parent) : QDialog(parent)
	, ui(new Ui::propertiesw)
	, smi(new settings)
{
	ui->setupUi(this);

	// set window size
	this->resize(360, 480);

	if (( int )smi->getValue("CoreApps", "UIMode") == 2)
	{
		setFixedSize(qApp->primaryScreen()->size() * .8);
	}

	ui->close->setIconSize(smi->getValue("CoreApps", "ToolsIconSize"));

	pathName = paths;
	info     = QFileInfo(pathName);

	permission();
	general();
	details();
	partition(pathName);
	show();

	connect(ui->close, SIGNAL(clicked()), this, SLOT(close()));
	this->setAttribute(Qt::WA_DeleteOnClose, 1);

	QFontMetrics fm(ui->name->font());
//	QFontMetrics fm( QFont( font().family(), 12, QFont::Bold ) );
	QRect textRect = fm.boundingRect(CPrime::FileUtils::baseName(pathName));
	QSize textSize = textRect.size();

	int pixSize = height();

	QPixmap pix(pixSize, pixSize);
	pix.fill(Qt::transparent);

	QTransform trans;

	QPainter painter(&pix);
	painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
	painter.setTransform(trans.translate(0, textSize.width()).rotate(-90));
	painter.setPen(ui->name->palette().color(QPalette::Text));
	painter.setFont(ui->name->font());
	painter.drawText(QRect(0, 0, pixSize, pixSize), Qt::AlignTop | Qt::AlignLeft, CPrime::FileUtils::baseName(pathName));
	painter.end();

	ui->name->setText("");
	ui->name->setPixmap(pix.copy(0, 0, textSize.height(), textSize.width()));
	ui->name->setToolTip(pathName);
}


propertiesw::~propertiesw()
{
	delete smi;
	delete ui;
}


void propertiesw::general()
{
	QFile         file(pathName);
	QMimeDatabase db;
	QMimeType     mime = db.mimeTypeForFile(info, QMimeDatabase::MatchContent);
	QString       littleinfo = info.suffix();
	QString       extrainfo = mime.name();
	QString       size, type, exc;

	this->setWindowTitle("Properties - " + info.fileName());

	if (info.isFile())
	{
		ui->type->setText(littleinfo + " , " + extrainfo);
		ui->labelContains->setVisible(false);
		ui->contains->setVisible(false);
	}

	if (info.isDir())
	{
		ui->type->setText("Directory");
	}

	SizeRunner *runner = new SizeRunner();

	connect(runner, SIGNAL(currentCount(quint64,quint64,quint64)), this, SLOT(update(quint64,quint64,quint64)));
	connect(runner, SIGNAL(finalCount(quint64,quint64,quint64)), this, SLOT(update(quint64,quint64,quint64)));

	runner->setPaths(QStringList() << pathName);
	runner->start();

//	QFontMetrics fm( ui->fileTitle->font() );
//	int maxwidth = static_cast<int>( qApp->primaryScreen()->availableGeometry().width() * .27 );
//	ui->fileTitle->setWordWrap( false );
//	ui->fileTitle->setText( fm.elidedText( info.fileName(), Qt::ElideMiddle, maxwidth ) );

	QFontMetrics fm       = QFontMetrics(ui->location->font());
	int          maxwidth = static_cast<int>(qApp->primaryScreen()->availableGeometry().width() * .27);

	ui->location->setText(fm.elidedText(info.path(), Qt::ElideLeft, maxwidth));

	ui->modified->setText(info.lastModified().toString());
	ui->created->setText(info.lastModified().toString());

	ui->owner->setText(info.owner());
	ui->group->setText(info.group());
	ui->executable->setText("File is not Executable");
	if (info.isExecutable() == true)
	{
		ui->executable->setText("File is Executable");
		ui->executableB->setChecked(1);
	}
}


void propertiesw::details()
{
	QStringList image, media;

	image << "jpg" << "jpeg" << "png" << "bmp" << "ico" << "svg" << "gif";
	media << "webm" << "mkv" << "flv" << "avi" << "mov" << "m4a"
		  << "mp4" << "3gp" << "wav" << "mp3" << "ogg" << "flac";

	QString suffix = info.suffix();

	//image
	if (image.contains(suffix, Qt::CaseInsensitive))
	{
		detailImage(pathName);
	}

	//not supported
	else
	{
		ui->propertiestab->removeTab(2);
	}
}


void propertiesw::permission()
{
	connect(ui->permissionsNumeric, SIGNAL(textChanged(QString)), this, SLOT(numericChanged(QString)));

	struct stat perms;
	stat(pathName.toLocal8Bit(), &perms);
	permString = QString("%1%2%3").arg(((perms.st_mode & S_IRWXU) >> 6)).arg(((perms.st_mode & S_IRWXG) >> 3)).arg((perms.st_mode & S_IRWXO));

	ui->permissionsNumeric->setText(permString);

	QRegularExpression          input("^[0-7]*$");
	QRegularExpressionValidator *permNumericValidator = new QRegularExpressionValidator(input, this);
	ui->permissionsNumeric->setValidator(permNumericValidator);
	ui->permissionsNumeric->setMaxLength(3);

	int ret = chmod(pathName.toLocal8Bit(), permString.toInt(0, 8));   //cast
	if (ret)
	{
		ui->permissions->setDisabled(1);
	}

	connect(ui->ownerRead, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->ownerWrite, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->ownerExec, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->groupRead, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->groupWrite, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->groupExec, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->otherRead, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->otherWrite, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));
	connect(ui->otherExec, SIGNAL(clicked()), this, SLOT(checkboxesChanged()));

	if (isExecutable(info.filePath()) == false)
	{
		ui->executableB->setVisible(0);
	}

	if (info.fileName().length())
	{
		if (info.fileName().at(0) == '.')
		{
			ui->hiddenB->setChecked(true);
		}
		else
		{
			ui->hiddenB->setChecked(false);
		}
	}
}


void propertiesw::partition(const QString path)
{
	// Function from utilities.cpp
	QString t = CPrime::FileUtils::formatSize(static_cast<quint64>(QStorageInfo(path).bytesTotal()));
	QString f = CPrime::FileUtils::formatSize(static_cast<quint64>(QStorageInfo(path).bytesAvailable()));

//    QString s = QString::number( ( double ) t - f );

	ui->sizefree->setText("Free : " + f);
	ui->sizetotal->setText("Total : " + t);
}


void propertiesw::detailImage(const QString imagepath)
{
	QImageReader reader(imagepath);
	const QImage image = reader.read();
	QFileInfo    info(imagepath);

	QStringList left;
	QStringList right;

	left << "Name" << "Size" << "Type" << "Dimensions" << "Bitplane Count"
		 << "Width" << "Height";
	right << info.fileName() << CPrime::FileUtils::formatSize(static_cast<quint64>(info.size())) << info.suffix().toUpper() // Function from utilities.cpp
		  << QString::number(image.width()) + " x " + QString::number(image.height())
		  << QString::number(image.bitPlaneCount()) << QString::number(image.width()) + " pixels"
		  << QString::number(image.height()) + " pixels";

	int     count = left.count();
	QString sep   = "\n";

	for (int i = 0; i < count; i++)
	{
		if (i + 1 == count)
		{
			sep = "";
		}

		ui->infoLeft->setText(ui->infoLeft->text() + left[i] + sep);
		ui->infoRight->setText(ui->infoRight->text() + ": " + right[i] + sep);
	}
}


void propertiesw::checkboxesChanged()
{
	ui->permissionsNumeric->setText(QString("%1%2%3").arg(ui->ownerRead->isChecked() * 4 + ui->ownerWrite->isChecked() * 2 + ui->ownerExec->isChecked())
									   .arg(ui->groupRead->isChecked() * 4 + ui->groupWrite->isChecked() * 2 + ui->groupExec->isChecked())
									   .arg(ui->otherRead->isChecked() * 4 + ui->otherWrite->isChecked() * 2 + ui->otherExec->isChecked()));
	applyPermission();
}


void propertiesw::numericChanged(QString text)
{
	if (text.length() != 3)
	{
		return;
	}

	int owner = QString(text.at(0)).toInt();
	ui->ownerRead->setChecked(owner / 4);
	ui->ownerWrite->setChecked((owner - owner / 4 * 4 - owner % 2));
	ui->ownerExec->setChecked(owner % 2);

	int group = QString(text.at(1)).toInt();
	ui->groupRead->setChecked(group / 4);
	ui->groupWrite->setChecked((group - group / 4 * 4 - group % 2));
	ui->groupExec->setChecked(group % 2);

	int other = QString(text.at(2)).toInt();
	ui->otherRead->setChecked(other / 4);
	ui->otherWrite->setChecked((other - other / 4 * 4 - other % 2));
	ui->otherExec->setChecked(other % 2);
}


void propertiesw::on_executableB_clicked(bool checked)
{
	ui->ownerExec->setChecked(checked);
	ui->groupExec->setChecked(checked);
	ui->otherExec->setChecked(checked);

	QString path = info.filePath();
	//Q_FOREACH( QString path, pathsList ) {
	QFile::Permissions perms = QFile::Permissions();

	if (ui->ownerRead->isChecked())
	{
		perms |= ((ui->ownerRead->checkState() == Qt::Checked) ? QFile::ReadOwner : (QFile::permissions(path) & QFile::ReadOwner));
	}

	if (ui->ownerWrite->isChecked())
	{
		perms |= ((ui->ownerWrite->checkState() == Qt::Checked) ? QFile::WriteOwner : (QFile::permissions(path) & QFile::WriteOwner));
	}

	if (ui->ownerExec->isChecked())
	{
		perms |= ((ui->ownerExec->checkState() == Qt::Checked) ? QFile::ExeOwner : (QFile::permissions(path) & QFile::ExeOwner));
	}

	if (ui->groupRead->isChecked())
	{
		perms |= ((ui->groupRead->checkState() == Qt::Checked) ? QFile::ReadGroup : (QFile::permissions(path) & QFile::ReadGroup));
	}

	if (ui->groupWrite->isChecked())
	{
		perms |= ((ui->groupWrite->checkState() == Qt::Checked) ? QFile::WriteGroup : (QFile::permissions(path) & QFile::WriteGroup));
	}

	if (ui->groupExec->isChecked())
	{
		perms |= ((ui->groupExec->checkState() == Qt::Checked) ? QFile::ExeGroup : (QFile::permissions(path) & QFile::ExeGroup));
	}

	if (ui->otherRead->isChecked())
	{
		perms |= ((ui->otherRead->checkState() == Qt::Checked) ? QFile::ReadOther : (QFile::permissions(path) & QFile::ReadOther));
	}

	if (ui->otherWrite->isChecked())
	{
		perms |= ((ui->otherWrite->checkState() == Qt::Checked) ? QFile::WriteOther : (QFile::permissions(path) & QFile::WriteOther));
	}

	if (ui->otherExec->isChecked())
	{
		perms |= ((ui->otherExec->checkState() == Qt::Checked) ? QFile::ExeOther : (QFile::permissions(path) & QFile::ExeOther));
	}

	QFile::setPermissions(path, perms);

	checkboxesChanged();
}


bool propertiesw::isExecutable(const QString path)
{
	QFileInfo   cinfo(path);
	QStringList type;

	type << "so" << "o" << "sh" << "deb" << "rpm" << "tar.gz"
		 << "tar" << "gz" << "ko" << "AppImage";

	QString suffix = cinfo.suffix();

	if (type.contains(suffix, Qt::CaseInsensitive))
	{
		return true;
	}

	return false;
}


void propertiesw::on_hiddenB_clicked(bool checked)
{
	if (checked)
	{
		QString path = info.path() + "/." + info.fileName();
		QFile(info.filePath()).rename(path);
		pathName = path;
		info     = QFileInfo(path);
	}
	else
	{
		if (info.fileName().at(0) == '.')
		{
			QString path = info.path() + "/" + info.fileName().remove(0, 1);
			QFile(info.filePath()).rename(path);
			pathName = path;
			info     = QFileInfo(path);
		}
	}

	general();
	ui->propertiestab->setCurrentIndex(1);
}


SizeAndCountText propertiesw::getF(const QStringList& paths)
{
	quint64 totalSize = 0;
	int     files     = 0;
	int     folders   = 0;
	int     hfCount   = 0;

	Q_FOREACH ( QString path, paths )
	{
		if (QFileInfo(path).isDir())
		{
			QDirIterator it(path, QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::NoSymLinks | QDir::Hidden, QDirIterator::Subdirectories);

			while (it.hasNext())
			{
				it.next();
				if (it.fileInfo().isHidden())
				{
					hfCount++;
				}

				else if (it.fileInfo().isDir())
				{
					if (it.filePath() == path)
					{
						continue;
					}

					folders++;
				}

				else if (it.fileInfo().isFile())
				{
					files++;
					totalSize += it.fileInfo().size();
				}
			}
		}

		else
		{
			if (QFileInfo(path).isHidden())
			{
				hfCount++;
			}

			files++;
			totalSize += CPrime::FileUtils::getFileSize(path);
		}
	}

	SizeAndCountText sc;

	sc.countText       = QString("%1 Files, %2 Folders").arg(files).arg(folders);
	sc.sizeText        = CPrime::FileUtils::formatSize(static_cast<quint64>(totalSize));
	sc.hiddenFileCount = QString("%1 Hidden").arg(hfCount);

	return sc;
}


void propertiesw::update(quint64 files, quint64 folders, quint64 totalSize)
{
	ui->size->setText(CPrime::FileUtils::formatSize(totalSize));

	QString contentsStr;

	/* Both files and folders */
	if (files and folders)
	{
		if ((files == 1) and (folders == 1))
		{
			contentsStr = QString(tr("1 file, 1 sub-folder"));
		}

		else if ((files > 1) and (folders == 1))
		{
			contentsStr = QString(tr("%1 files, 1 sub-folder")).arg(files);
		}

		else if ((files == 1) and (folders > 1))
		{
			contentsStr = QString(tr("1 file, %2 sub-folders")).arg(folders);
		}

		else
		{
			contentsStr = QString(tr("%1 files, %2 sub-folders")).arg(files).arg(folders);
		}
	}

	/* Only files and no folders */
	else if (files and !folders)
	{
		if (files == 1)
		{
			contentsStr = QString(tr("%1 file")).arg(files);
		}

		else
		{
			contentsStr = QString(tr("%1 files")).arg(files);
		}
	}

	/* No files */
	else
	{
		/* No folders as well */
		if (not folders)
		{
			contentsStr = "Empty";
		}

		/* One folder */
		else if (folders == 1)
		{
			contentsStr = "1 sub-folder";
		}

		/* n folders */
		else
		{
			contentsStr = QString(tr("%1 sub-folders")).arg(folders);
		}
	}

	ui->contains->setText(contentsStr);
}


void propertiesw::applyPermission()
{
	bool ur = ui->ownerRead->isChecked();
	bool uw = ui->ownerWrite->isChecked();
	bool ux = ui->ownerExec->isChecked();

	bool gr = ui->groupRead->isChecked();
	bool gw = ui->groupWrite->isChecked();
	bool gx = ui->groupExec->isChecked();

	bool or_ = ui->otherRead->isChecked();
	bool ow  = ui->otherWrite->isChecked();
	bool ox  = ui->otherExec->isChecked();

	QFile::Permissions dxPerms = QFile::Permissions();

	if (ur)
	{
		dxPerms |= QFile::ReadUser;
	}
	if (uw)
	{
		dxPerms |= QFile::WriteUser;
	}
	if (ux)
	{
		dxPerms |= QFile::ExeUser;
	}

	if (gr)
	{
		dxPerms |= QFile::ReadGroup;
	}
	if (gw)
	{
		dxPerms |= QFile::WriteGroup;
	}
	if (gx)
	{
		dxPerms |= QFile::ExeGroup;
	}

	if (or_)
	{
		dxPerms |= QFile::ReadOther;
	}
	if (ow)
	{
		dxPerms |= QFile::WriteOther;
	}
	if (ox)
	{
		dxPerms |= QFile::ExeOther;
	}

	QFile(pathName).setPermissions(dxPerms);
}
