/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/


#include "folderview.h"

#include <QScroller>
#include <QStackedWidget>
#include <QLabel>
#include <QLayout>
#include <QPainter>

#include <cprime/filefunc.h>

// Init moveItems
bool FolderView::moveItems = false;

FolderView::FolderView(int viewMode, QWidget *parent)
	: QWidget(parent)
{
	setupUI();
	makeConnections();

	refreshView(viewMode);
}


void FolderView::setupUI()
{
	viewStack = new QStackedWidget(this);
	viewStack->layout()->setContentsMargins(QMargins());
	viewStack->layout()->setSpacing(0);

	fsm = new FileSystemModel(this);

	view = new FileSystemView(this);
	view->setModel(fsm);
	viewStack->addWidget(view);


	view2 = new DetailsSystemView(this);
	view2->setModel(fsm);
	viewStack->addWidget(view2);

	QScroller::grabGesture(view, QScroller::LeftMouseButtonGesture);
	QScroller::grabGesture(view2, QScroller::LeftMouseButtonGesture);

	QVBoxLayout *viewLyt = new QVBoxLayout();
	viewLyt->setContentsMargins(QMargins());
	viewLyt->setSpacing(0);

	viewLyt->addWidget(viewStack);

	setLayout(viewLyt);

	emit updateVarious();
}


void FolderView::makeConnections()
{
	/* Open current index */
	connect(view, SIGNAL(activated(const QModelIndex&)), this, SLOT(changeDir(const QModelIndex&)));
	connect(view2, SIGNAL(activated(const QModelIndex&)), this, SLOT(changeDir(const QModelIndex&)));

	connect(fsm, SIGNAL(directoryLoaded(QString)), this, SIGNAL(updateVarious()));

	/* Context menu: forward the signal to main window */
	connect(view, SIGNAL(customContextMenuRequested(const QPoint&)), this, SIGNAL(contextMenu(const QPoint&)));
	connect(view2, SIGNAL(customContextMenuRequested(const QPoint&)), this, SIGNAL(contextMenu(const QPoint&)));

	connect(view, SIGNAL(gotFocus()), this, SLOT(emitUpdateAddress()));
	connect(view2, SIGNAL(gotFocus()), this, SLOT(emitUpdateAddress()));

	/* Selections */
	connect(view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FolderView::updateVarious);
	connect(view2->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FolderView::updateVarious);
}


bool FolderView::hasFocus()
{
	return(view->hasFocus() | view2->hasFocus());
}


QStringList FolderView::selection()
{
	QModelIndexList list;

	if (viewStack->currentIndex() == 0)
	{
		list = view->selectionModel()->selectedIndexes();
	}

	else if (viewStack->currentIndex() == 1)
	{
		list = view2->selectionModel()->selectedIndexes();
	}

	else
	{
		return QStringList();
	}

	QStringList selected;

	Q_FOREACH ( QModelIndex idx, list )
	{
		selected << fsm->filePath(idx);
	}

	selected.removeDuplicates();

	return selected;
}


void FolderView::clearSelection()
{
	QModelIndexList list;

	view->selectionModel()->clear();
	view2->selectionModel()->clear();
}


QString FolderView::rootPath()
{
	return fsm->rootPath();
}


void FolderView::setIconSize(QSize iconView, QSize listView)
{
	view->setIconSize(iconView);
	view2->setIconSize(listView);
}


void FolderView::sort(int column, Qt::SortOrder order)
{
	fsm->sort(column, order);
}


void FolderView::refresh()
{
	fsm->reload();
}


bool FolderView::canGoBack()
{
	return(curIndex > 0);
}


bool FolderView::canGoForward()
{
	return(curIndex < (oldRoots.count() - 1));
}


bool FolderView::canGoUp()
{
	return(fsm->rootPath() != "/");
}


void FolderView::selectAll()
{
	// if ( viewStack->currentIndex() == 0 )
	view->selectAll();

	// else
	view2->selectAll();
}


void FolderView::goBack()
{
	if (curIndex > 0)
	{
		curIndex--;
		changeDir(oldRoots.at(curIndex));
	}
}


void FolderView::goForward()
{
	if (curIndex < (oldRoots.count() - 1))
	{
		curIndex++;
		changeDir(oldRoots.at(curIndex));
	}
}


void FolderView::goUp()
{
	if (fsm->rootPath() == "/")
	{
		return;
	}

	if (oldRoots.count())
	{
		oldRoots.erase(oldRoots.begin() + curIndex + 1, oldRoots.end());
	}

	/* Append this root to navigation list */
	oldRoots << CPrime::FileUtils::dirName(fsm->rootPath());
	curIndex = oldRoots.count() - 1;

	changeDir(oldRoots.value(curIndex));
}


void FolderView::loadAddress(QString addr)
{
	/* If we came here via @openAct */
	if ((not addr.length()) and selection().length())
	{
		addr = selection().value(0);
	}

	/* Add to history only if addr is a directory */
	if (CPrime::FileUtils::isDir(addr))
	{
		if (oldRoots.count())
		{
			oldRoots.erase(oldRoots.begin() + curIndex + 1, oldRoots.end());
		}

		/* Append this root to navigation list */
		oldRoots << addr;
		curIndex = oldRoots.count() - 1;
	}

	changeDir(addr);
}


void FolderView::changeDir(const QModelIndex& idx)
{
	if (not selection().contains(fsm->filePath(idx)))
	{
		return;
	}

	/* Add to history only if addr is a directory */
	if (CPrime::FileUtils::isDir(fsm->filePath(idx)))
	{
		if (oldRoots.count())
		{
			oldRoots.erase(oldRoots.begin() + curIndex + 1, oldRoots.end());
		}

		/* Append this root to navigation list */
		oldRoots << fsm->filePath(idx);
		curIndex = oldRoots.count() - 1;
	}

	changeDir(fsm->filePath(idx));
}


void FolderView::changeDir(QString path)
{
	QFileInfo info(path);

	if (info.isDir())
	{
		FileSystemModel::iconMap.clear();

		view->setRootIndex(fsm->setRootPath(info.absoluteFilePath()));
		view2->setRootIndex(fsm->setRootPath(info.absoluteFilePath()));

		view->selectionModel()->clear();
		view2->selectionModel()->clear();

		emit updateVarious();
	}

	else
	{
		// mimeUtils->openInApp( path, this );
		emit openFile(path);
	}
}


void FolderView::refreshView(int viewMode)
{
	disconnect(view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FolderView::updateVarious);
	disconnect(view2->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FolderView::updateVarious);

	/* IconView */
	if (viewMode == 0)
	{
		QItemSelection selection = view2->selectionModel()->selection();
		view->selectionModel()->select(selection, QItemSelectionModel::ClearAndSelect);

		viewStack->setCurrentIndex(0);
		view->setIconView();
	}

	/* TileView */
	else if (viewMode == 1)
	{
		QItemSelection selection = view2->selectionModel()->selection();
		view->selectionModel()->select(selection, QItemSelectionModel::ClearAndSelect);

		viewStack->setCurrentIndex(0);
		view->setTileView();
	}

	/* DetailsView */
	else
	{
		QItemSelection selection = view->selectionModel()->selection();
		view2->selectionModel()->select(selection, QItemSelectionModel::ClearAndSelect);

		viewStack->setCurrentIndex(1);
	}

	connect(view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FolderView::updateVarious);
	connect(view2->selectionModel(), &QItemSelectionModel::selectionChanged, this, &FolderView::updateVarious);
}


void FolderView::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);

	painter.fillRect(rect(), Qt::transparent);

	if (view->hasFocus() or view2->hasFocus())
	{
		painter.setPen(Qt::gray);
		painter.drawRect(rect().adjusted(0, 0, -1, -1));
	}

	painter.end();

	QWidget::paintEvent(pEvent);
}
