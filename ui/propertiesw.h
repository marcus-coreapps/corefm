/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#pragma once

#include <QImageReader>
#include <QThread>
#include <QDialog>

#include "global.h"


namespace Ui {
class propertiesw;
}

struct SizeAndCountText
{
	QString sizeText, countText, hiddenFileCount;
};

class SizeRunner : public QThread {
	Q_OBJECT

public:
	SizeRunner() : QThread()
	{
		mTerminate = false;
	}

	void setPaths(QStringList paths)
	{
		mPaths.clear();
		mPaths << paths;
	}

	void start()
	{
		files     = 0;
		folders   = 0;
		totalSize = 0;

		mTerminate = false;
		QThread::start();
	}

	void stop()
	{
		mTerminate = true;
	}

protected:
	void run();

private:
	QStringList mPaths;
	bool mTerminate;

	quint64 files = 0, folders = 0, totalSize = 0;

	quint64 fileSize(QString path);
	void recurseProperties(QString path);

Q_SIGNALS:
	void currentCount(quint64, quint64, quint64);
	void finalCount(quint64, quint64, quint64);
};

class propertiesw : public QDialog {
	Q_OBJECT

public:
	explicit propertiesw(const QString paths, QWidget *parent = nullptr);
	~propertiesw();

public slots:
	void checkboxesChanged();
	void numericChanged(QString);

private slots:
	void on_executableB_clicked(bool checked);
	void on_hiddenB_clicked(bool checked);
	void update(quint64, quint64, quint64);
	void detailImage(const QString imagepath);
	void applyPermission();

private:
	Ui::propertiesw *ui;
	QString pathName, permString;
	QFileInfo info;
	settings *smi;

	void permission();
	void general();
	void details();
	void partition(const QString path);
	bool isExecutable(const QString path);
	static SizeAndCountText getF(const QStringList&);
};
