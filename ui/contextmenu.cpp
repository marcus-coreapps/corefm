/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/


#include "corefm.h"
#include "folderview.h"

#include <QMenu>

#include <cprime/themefunc.h>

void corefm::contextMenu(const QPoint&)
{
	QMenu *menu = new QMenu(currentView());

	menu->setAttribute(Qt::WA_DeleteOnClose);

	QStringList selectedList;

	selectedList = currentView()->selection();

	if (selectedList.isEmpty())
	{
		// Create a new file/directory
		QMenu *createNewMenu = new QMenu("Create New");
		createNewMenu->setIcon(QIcon::fromTheme("archive-insert"));

		createNewMenu->addAction(newFolderAct);
		createNewMenu->addAction(newFileAct);

		// Special actions
		QMenu *specialMenu = new QMenu("Specials");
		specialMenu->addAction(treeWriteAct);

		menu->addMenu(createNewMenu);
		menu->addMenu(specialMenu);
		menu->addSeparator();

		menu->addAction(termAct);
		menu->addSeparator();

		menu->addAction(pasteAct);
		menu->addSeparator();

		menu->addAction(refreshAct);
		menu->addSeparator();

		menu->addAction(selectAllAct);
		menu->addSeparator();

		menu->addSeparator();
		menu->addAction(propsAct);
	}

	else if (selectedList.count() == 1)
	{
		menu->addAction(openAct);

		QFileInfo fInfo(selectedList[0]);

		if (fInfo.isDir())
		{
			menu->addAction(newWindowAct);
			menu->addAction(openInSplitAct);
			menu->addSeparator();
		}

		menu->addMenu(openWithMenu());

		// Special actions
		QMenu *specialMenu = new QMenu("Specials");
		if (fInfo.isDir())
		{
			specialMenu->addAction(treeWriteAct);
			specialMenu->addAction(batchRenameAct);
			menu->addMenu(specialMenu);
		}

		if (fInfo.isFile())
		{
			specialMenu->addAction(metadataAct);
			specialMenu->addAction(batchRenameAct);
			menu->addMenu(specialMenu);
		}
		menu->addSeparator();

		if (fInfo.isDir())
		{
			menu->addAction(termAct);
			menu->addSeparator();
		}

		menu->addAction(cutAct);
		menu->addAction(copyAct);
		menu->addSeparator();
		menu->addAction(renameAct);
		menu->addSeparator();
		menu->addAction(trashAct);
		menu->addAction(deleteAct);
		menu->addSeparator();
		menu->addAction(propsAct);
	}

	else
	{
		menu->addMenu(openWithMenu());
		menu->addSeparator();
		menu->addAction(cutAct);
		menu->addAction(copyAct);
		menu->addSeparator();
		menu->addAction(trashAct);
		menu->addAction(deleteAct);
		menu->addSeparator();
		menu->addAction(propsAct);
	}

	menu->exec(QCursor::pos());
}


QMenu * corefm::openWithMenu()
{
	QMenu       *openMenu     = new QMenu(tr("Open with"), this);
	QStringList selectedItems = currentView()->selection();

	// Select action
	QAction *selectAppAct = new QAction(tr("Select..."), openMenu);

	connect(selectAppAct, SIGNAL(triggered()), this, SLOT(selectApp()));

	// Load default applications for current mime
	CPrime::AppsList apps = mimeH->appsForMimeType(mimeDbInstance.mimeTypeForFile(selectedItems.value(0)));

	// Create actions for opening
	QList<QAction *> defaultApps;

	Q_FOREACH ( CPrime::DesktopFile app, apps )
	{
		QString name = app.name();
		QIcon   icon = CPrime::ThemeFunc::getAppIcon(app.desktopName());

		// Create action
		QAction *action = new QAction(icon, name, openMenu);
		action->setData(QVariant::fromValue<CPrime::DesktopFile>(app));
		defaultApps.append(action);

		// TODO: icon and connect
		connect(action, SIGNAL(triggered()), SLOT(openInApp()));

		// Add action to menu
		openMenu->addAction(action);
	}

	// Add open action to menu
	openMenu->addSeparator();
	openMenu->addAction(selectAppAct);
	return openMenu;
}
