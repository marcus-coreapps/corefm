/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QMenu>
#include <QListWidgetItem>
#include <QClipboard>
#include <QScroller>
#include <QCloseEvent>
#include <QFileDialog>
#include <QScreen>

#include <dirent.h>

#include <cprime/ioprocesses.h>
#include <cprime/applicationdialog.h>
#include <cprime/filefunc.h>
#include <cprime/activitesmanage.h>
#include <cprime/appopenfunc.h>
#include <cprime/trashmanager.h>
#include <cprime/pinit.h>
#include <cprime/shareit.h>
#include <cprime/messageengine.h>
#include <cprime/themefunc.h>
#include <cprime/desktopfile.h>

#include <csys/storageinfo.h>

#include "filedialog.h"
#include "folderview.h"
#include "corefm.h"
#include "ui_corefm.h"

// Init moveItems
bool corefm::moveItems = false;

corefm::corefm(QString path, QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::corefm)
	, showHidden(false)
	, showThumb(false)
	, isSplit(false)
	, uiMode(0)
	, viewMode(1)
	, toolsIconSize(48, 48)
	, listViewIconSize(48, 48)
	, iconViewIconSize(56, 56)
	, clipBoard(QApplication::clipboard())
	, mimeH(CPrime::SystemXdgMime::instance())
	, sMgr(new StorageManager)
	, smi(new settings)
{
	ui->setupUi(this);

	loadSettings();

	view1 = new FolderView(viewMode, this);
	view2 = new FolderView(viewMode, this);
	view1->setObjectName("view1");
	view2->setObjectName("view2");
	view1->setFocusPolicy(Qt::StrongFocus);
	view2->setFocusPolicy(Qt::StrongFocus);

	QScroller::grabGesture(ui->pageTrash, QScroller::LeftMouseButtonGesture);
	QScroller::grabGesture(ui->driveView->viewport(), QScroller::LeftMouseButtonGesture);

	QVariant            OvershootPolicy    = QVariant::fromValue<QScrollerProperties::OvershootPolicy>(QScrollerProperties::OvershootAlwaysOff);
	QScrollerProperties ScrollerProperties = QScroller::scroller(ui->driveView->viewport())->scrollerProperties();
	ScrollerProperties.setScrollMetric(QScrollerProperties::VerticalOvershootPolicy, OvershootPolicy);
	ScrollerProperties.setScrollMetric(QScrollerProperties::HorizontalOvershootPolicy, OvershootPolicy);
	QScroller::scroller(ui->driveView->viewport())->setScrollerProperties(ScrollerProperties);

	ui->splitter->addWidget(view1);
	ui->splitter->addWidget(view2);

	path = QFileInfo(path).absoluteFilePath();

	startSetup();
	setupActions();
	setupIcons();
	makeConnections();

	view1->loadAddress(path);
	view2->loadAddress(path);
	view2->hide();

	view1->activateWindow();
	view1->setFocus();
}


corefm::~corefm()
{
	delete sMgr;
	delete smi;
	delete ui;
}


/**
  * @brief Setup ui elements
  */
void corefm::startSetup()
{
	// all toolbuttons icon size in sideView
	Q_FOREACH ( auto b, ui->sideView->findChildren<QToolButton *>())
	{
		if (b)
		{
			b->setIconSize(toolsIconSize);
		}
	}

	// all toolbuttons icon size in toolsBar
	Q_FOREACH ( auto b, ui->toolsBar->findChildren<QToolButton *>())
	{
		if (b)
		{
			b->setIconSize(toolsIconSize);
		}
	}

	// all toolbuttons icon size in topBar
	Q_FOREACH ( auto b, ui->topBar->findChildren<QToolButton *>())
	{
		if (b)
		{
			b->setIconSize(toolsIconSize);
		}
	}

	view1->setIconSize(iconViewIconSize, listViewIconSize);
	view2->setIconSize(iconViewIconSize, listViewIconSize);

	ui->toolsBar->setVisible(0);

	ui->menu->setVisible(0);
	ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
	ui->appTitle->setFocusPolicy(Qt::NoFocus);
	this->resize(800, 500);

	connect(ui->driveView, &QListWidget::itemClicked, this, &corefm::openSelecteDrive);

	if (CPrime::FileUtils::exists(QDir::home().filePath("Downloads")))
	{
		ui->downloads->setVisible(1);
	}
	else
	{
		ui->downloads->setVisible(0);
	}

	if (CPrime::FileUtils::exists(QDir::home().filePath("Desktop")))
	{
		ui->desktop->setVisible(1);
	}
	else
	{
		ui->desktop->setVisible(0);
	}


	if (uiMode == 2)
	{
		// setup mobile UI
		this->setWindowState(Qt::WindowMaximized);

		ui->menu->setIconSize(toolsIconSize);

		connect(ui->menu, &QToolButton::clicked, this, &corefm::showSideView);
		connect(ui->appTitle, &QToolButton::clicked, this, &corefm::showSideView);

		ui->sideView->setVisible(0);
		ui->menu->setVisible(1);

		ui->sizeLbl->setVisible(0);
		ui->spaceLbl->setVisible(0);
	}
	else
	{
		// setup desktop or tablet UI

		if (windowMaximized)
		{
			this->setWindowState(Qt::WindowMaximized);
			qDebug() << "window is maximized";
		}
		else
		{
			this->resize(windowSize);
		}
	}

	connect(ui->pageTrash, &TrashView::updateItemCount, [ = ](int count) {
		if (ui->stackedWidget->currentIndex() == 1)
		{
			ui->sizeLbl->setText(QString("%1 item%2").arg(count).arg(count > 1 ? "s" : ""));
		}
	});
}


/**
  * @brief Loads application settings
  */
void corefm::loadSettings()
{
	// get CSuite's settings
	uiMode           = smi->getValue("CoreApps", "UIMode");
	toolsIconSize    = smi->getValue("CoreApps", "ToolsIconSize");
	listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
	iconViewIconSize = smi->getValue("CoreApps", "IconViewIconSize");
	disableTrashConfirmationMessage = smi->getValue("CoreApps", "DisableTrashConfirmationMessage");

	// get app's settings
	showHidden      = smi->getValue("CoreFM", "ShowHidden");
	viewMode        = int( smi->getValue("CoreFM", "ViewMode"));
	sortMode        = int( smi->getValue("CoreFM", "SortMode"));
	showThumb       = smi->getValue("CoreFM", "ShowThumb");
	windowSize      = smi->getValue("CoreFM", "WindowSize");
	windowMaximized = smi->getValue("CoreFM", "WindowMaximized");
}


void corefm::setupIcons()
{
	ui->tools->setIcon(CPrime::ThemeFunc::themeIcon("view-more-symbolic", "tools-wizard", "view-more-symbolic"));
	ui->root->setIcon(CPrime::ThemeFunc::themeIcon("folder-root", "system-lock-screen-symbolic", "folder-root"));
	ui->pinIt->setIcon(CPrime::ThemeFunc::themeIcon("bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new"));
	ui->shareIt->setIcon(CPrime::ThemeFunc::themeIcon("document-send-symbolic", "document-send-symbolic", "document-send"));
	ui->trash->setIcon(CPrime::ThemeFunc::themeIcon("user-trash-symbolic", "user-trash", "user-trash"));
	ui->home->setIcon(CPrime::ThemeFunc::themeIcon("user-home-symbolic", "user-home", "user-home"));
	ui->downloads->setIcon(CPrime::ThemeFunc::themeIcon("folder-download-symbolic", "folder-download", "folder-download"));
	ui->desktop->setIcon(CPrime::ThemeFunc::themeIcon("user-desktop-symbolic", "user-desktop", "user-desktop"));
	ui->drives->setIcon(CPrime::ThemeFunc::themeIcon("drive-multidisk-symbolic", "drive-multidisk", "drive-multidisk"));
	ui->menu->setIcon(CPrime::ThemeFunc::themeIcon("open-menu-symbolic", "application-menu", "open-menu"));
}


void corefm::closeEvent(QCloseEvent *event)
{
	event->ignore();
	qDebug() << "save window stats" << this->size() << this->isMaximized();

	smi->setValue("CoreFM", "WindowSize", this->size());
	smi->setValue("CoreFM", "WindowMaximized", this->isMaximized());

	QStringList currentFile;

	if (currentView()->selection().count())
	{
		currentFile = currentView()->selection();
	}

	else
	{
		currentFile = QStringList() << currentView()->rootPath();
	}

	CPrime::ActivitiesManage::saveToActivites("corefm", currentFile);
	event->accept();
}


void corefm::showSideView()
{
	if (ui->sideView->isVisible())
	{
		ui->sideView->setVisible(0);
	}
	else
	{
		ui->sideView->setVisible(1);
	}
}


void corefm::setupActions()
{
	newFolderAct = new QAction(CPrime::ThemeFunc::themeIcon("folder-new-symbolic", "folder-new", "folder-new"), "New Folder", this);
	newFolderAct->setShortcut(tr("F10"));
	connect(newFolderAct, SIGNAL(triggered()), this, SLOT(newFolder()));

	newFileAct = new QAction(CPrime::ThemeFunc::themeIcon("document-new-symbolic", "document-new", "document-new"), "New Text", this);
	newFileAct->setShortcut(tr("Shift+F10"));
	connect(newFileAct, SIGNAL(triggered()), this, SLOT(newFile()));

	showHiddenAct = new QAction(CPrime::ThemeFunc::themeIcon("view-reveal-symbolic", "password-show-on", "dialog-password"), "Hidden", this);
	showHiddenAct->setShortcut(tr("Ctrl+H"));
	connect(showHiddenAct, SIGNAL(triggered()), this, SLOT(toggleHidden()));
	showHiddenAct->setCheckable(true);
	showHiddenAct->setChecked(showHidden);

	previewAct = new QAction(CPrime::ThemeFunc::themeIcon("folder-pictures-symbolic", "bitmap-trace", "bitmap-trace"), "Toggle Previews", this);
	previewAct->setShortcut(tr("Ctrl+Shift+I"));
	connect(previewAct, SIGNAL(triggered()), this, SLOT(togglePreviews()));
	previewAct->setCheckable(true);
	previewAct->setChecked(showThumb);

	termAct = new QAction(CPrime::ThemeFunc::themeIcon("utilities-terminal-symbolic", "utilities-terminal", "utilities-terminal"), "Open Terminal Here", this);
	termAct->setShortcut(tr("F4"));
	connect(termAct, SIGNAL(triggered()), this, SLOT(openTerminal()));

	cutAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-cut-symbolic", "edit-cut", "edit-cut"), "Cut", this);
	cutAct->setShortcut(tr("Ctrl+X"));
	connect(cutAct, SIGNAL(triggered()), this, SLOT(prepareMove()));

	copyAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-copy-symbolic", "edit-copy", "edit-copy"), "Copy", this);
	copyAct->setShortcut(tr("Ctrl+C"));
	connect(copyAct, SIGNAL(triggered()), this, SLOT(prepareCopy()));

	pasteAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-paste-symbolic", "edit-paste", "edit-paste"), "Paste", this);
	pasteAct->setShortcut(tr("Ctrl+V"));
	connect(pasteAct, SIGNAL(triggered()), this, SLOT(paste()));

	openAct = new QAction(CPrime::ThemeFunc::themeIcon("document-open-symbolic", "quickopen-file", "document-open-symbolic"), "Open", this);
	openAct->setShortcuts(QList<QKeySequence>() << tr("Enter") << tr("Return"));
	connect(openAct, SIGNAL(triggered()), this, SLOT(loadAddress()));

	refreshAct = new QAction(CPrime::ThemeFunc::themeIcon("view-refresh-symbolic", "view-refresh", "view-refresh"), "Refresh", this);
	refreshAct->setShortcut(tr("F5"));
	connect(refreshAct, SIGNAL(triggered()), this, SLOT(refresh()));

	selectAllAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-select-all-symbolic", "edit-select-all", "edit-select-all"), "Select All", this);
	selectAllAct->setShortcut(tr("Ctrl+A"));
	connect(selectAllAct, SIGNAL(triggered()), this, SLOT(selectAll()));

	renameAct = new QAction(CPrime::ThemeFunc::themeIcon("document-edit-symbolic", "edit-rename", "document-edit"), "Rename", this);
	renameAct->setShortcut(tr("F2"));
	connect(renameAct, SIGNAL(triggered()), this, SLOT(rename()));

	batchRenameAct = new QAction(CPrime::ThemeFunc::themeIcon("document-edit-symbolic", "edit-rename", "document-edit"), "Batch Rename", this);
	batchRenameAct->setShortcut(tr("Shift+F2"));
	connect(batchRenameAct, SIGNAL(triggered()), this, SLOT(openRenamer()));

	trashAct = new QAction(CPrime::ThemeFunc::themeIcon("user-trash-symbolic", "edit-delete", "edit-delete"), "Move to Trash", this);
	trashAct->setShortcut(tr("Delete"));
	connect(trashAct, SIGNAL(triggered()), this, SLOT(trashSelected()));

	deleteAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-delete-symbolic", "edit-delete", "edit-delete"), "Delete", this);
	deleteAct->setShortcut(tr("Shift+Delete"));
	connect(deleteAct, SIGNAL(triggered()), this, SLOT(deleteSelected()));

	propsAct = new QAction(CPrime::ThemeFunc::themeIcon("document-properties-symbolic", "document-properties", "document-properties"), "Properties", this);
	propsAct->setShortcut(tr("Alt+Return"));
	connect(propsAct, SIGNAL(triggered()), this, SLOT(showProperties()));

	backAct = new QAction(CPrime::ThemeFunc::themeIcon("go-next-symbolic", "go-next", "go-next"), "Previous", this);
	backAct->setShortcut(tr("Alt+Left"));
	connect(backAct, SIGNAL(triggered()), this, SLOT(goBack()));

	openWithAct = new QAction(CPrime::ThemeFunc::themeIcon("document-open-symbolic", "quickopen-file", "document-open-symbolic"), "Open With", this);
	openWithAct->setShortcut(tr("Ctrl+Return"));
	connect(openWithAct, SIGNAL(triggered()), this, SLOT(selectApp()));

	upAct = new QAction(CPrime::ThemeFunc::themeIcon("go-previous-symbolic", "go-previous", "go-previous"), "Parent dir", this);
	upAct->setShortcut(tr("Backspace"));
	connect(upAct, SIGNAL(triggered()), this, SLOT(goUp()));

	splitAct = new QAction(CPrime::ThemeFunc::themeIcon("view-split-left-right", "view-dual-symbolic", "view-split-left-right"), "Toggle Split View", this);
	connect(splitAct, SIGNAL(triggered()), this, SLOT(toggleSplit()));
	splitAct->setCheckable(true);
	splitAct->setChecked(false);

	openInSplitAct = new QAction(CPrime::ThemeFunc::themeIcon("view-split-left-right", "view-dual-symbolic", "view-split-left-right"), "Open folder in SplitView", this);
	openInSplitAct->setShortcut(tr("Ctrl+Shift+Return"));
	connect(openInSplitAct, SIGNAL(triggered()), this, SLOT(openInSplitView()));

	viewChangeAct = new QAction(CPrime::ThemeFunc::themeIcon("view-grid-symbolic", "view-grid", "view-grid"), "Change View", this);
	viewChangeAct->setShortcut(tr("Ctrl+Shift+V"));
	connect(viewChangeAct, SIGNAL(triggered()), this, SLOT(nextView()));
	nextView();

	sortChangeAct = new QAction(CPrime::ThemeFunc::themeIcon("view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"), "Sort", this);
	connect(sortChangeAct, SIGNAL(triggered()), this, SLOT(nextSortMode()));
	sortChangeAct->setShortcut(tr("Ctrl+Shift+S"));
	nextSortMode();

	newWindowAct = new QAction(CPrime::ThemeFunc::themeIcon("window-new-symbolic", "window-new", "window-new"), "New Window", this);
	newWindowAct->setShortcut(tr("Ctrl+n"));
	connect(newWindowAct, SIGNAL(triggered()), this, SLOT(newWindow()));

	treeWriteAct = new QAction(CPrime::ThemeFunc::themeIcon("document-new-symbolic", "document-new", "document-new"), "Save Dir Tree as text", this);
	connect(treeWriteAct, SIGNAL(triggered()), this, SLOT(writeTree()));

	metadataAct = new QAction(CPrime::ThemeFunc::themeIcon("document-properties-symbolic", "document-properties", "document-properties"), "Open Metadata viewer", this);
	connect(metadataAct, SIGNAL(triggered()), this, SLOT(openMetadataViewer()));

	searchAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-find-symbolic", "edit-find", "edit-find"), "Search", this);
	connect(searchAct, SIGNAL(triggered()), this, SLOT(openSearch()));

	ui->back->setDefaultAction(backAct);
	ui->up->setDefaultAction(upAct);
	ui->cut->setDefaultAction(cutAct);
	ui->copy->setDefaultAction(copyAct);
	ui->paste->setDefaultAction(pasteAct);
	ui->trashit->setDefaultAction(trashAct);
	ui->properties->setDefaultAction(propsAct);
	ui->newfolder->setDefaultAction(newFolderAct);
	ui->newtext->setDefaultAction(newFileAct);
	ui->showHidden->setDefaultAction(showHiddenAct);
	ui->showthumb->setDefaultAction(previewAct);
	ui->terminal->setDefaultAction(termAct);
	ui->split->setDefaultAction(splitAct);
	ui->views->setDefaultAction(viewChangeAct);
	ui->selectAll->setDefaultAction(selectAllAct);
	ui->openWith->setDefaultAction(openWithAct);
	ui->search->setDefaultAction(searchAct);
	ui->rename->setDefaultAction(renameAct);
	ui->sort->setDefaultAction(sortChangeAct);

	addAction(newFolderAct);
	addAction(newFileAct);
	addAction(showHiddenAct);
	addAction(previewAct);
	addAction(termAct);
	addAction(cutAct);
	addAction(copyAct);
	addAction(pasteAct);
	addAction(openAct);
	addAction(refreshAct);
	addAction(selectAllAct);
	addAction(renameAct);
	addAction(trashAct);
	addAction(deleteAct);
	addAction(propsAct);
	addAction(batchRenameAct);
	addAction(newWindowAct);
	addAction(backAct);
	addAction(upAct);
	addAction(splitAct);
	addAction(openInSplitAct);
	addAction(viewChangeAct);
	addAction(treeWriteAct);
	addAction(metadataAct);
	addAction(searchAct);
	addAction(sortChangeAct);
//	addAction( zoomInAct );
//	addAction( zoomOutAct );
}


void corefm::makeConnections()
{
	connect(view1, SIGNAL(contextMenu(const QPoint&)), this, SLOT(contextMenu(const QPoint&)));
	connect(view2, SIGNAL(contextMenu(const QPoint&)), this, SLOT(contextMenu(const QPoint&)));

	connect(view1, SIGNAL(updateVarious()), this, SLOT(updateVarious()));
	connect(view2, SIGNAL(updateVarious()), this, SLOT(updateVarious()));

	connect(view1, SIGNAL(openFile(QString)), this, SLOT(openInApp(QString)));
	connect(view2, SIGNAL(openFile(QString)), this, SLOT(openInApp(QString)));

//    connect( view1, &FolderView::updateAddress, ui->pathLbl, &QLabel::setText );
//    connect( view2, &FolderView::updateAddress, ui->pathLbl, &QLabel::setText );
}


void corefm::on_tools_clicked()
{
	// first hide the sideview to get more spaces
	if (uiMode == 2)
	{
		ui->sideView->setVisible(0);
	}

	if ((ui->stackedWidget->currentIndex() == 1) || (ui->stackedWidget->currentIndex() == 2))
	{
		return;
	}

	if (ui->toolsBar->isVisible())
	{
		ui->toolsBar->hide();
	}
	else
	{
		ui->toolsBar->show();
	}
}


FolderView * corefm::currentView()
{
	if (not view2->isVisible())
	{
		return view1;
	}

	if (view2->hasFocus())
	{
		return view2;
	}
	else
	{
		return view1;
	}
}


void corefm::newFolder()
{
	QString path = currentView()->rootPath();

	filedialog dlg(filedialog::NewFolder, path, QString(), this);

	dlg.exec();
}


void corefm::newFile()
{
	QString path = currentView()->rootPath();

	filedialog dlg(filedialog::NewFile, path, QString(), this);

	dlg.exec();
}


void corefm::toggleHidden()
{
	showHidden = not showHidden;
	smi->setValue("CoreFM", "ShowHidden", showHidden);
	currentView()->refresh();
}


void corefm::togglePreviews()
{
	showThumb = not showThumb;
	smi->setValue("CoreFM", "ShowThumb", showThumb);
	currentView()->refresh();
}


void corefm::openTerminal()
{
	QStringList selectedList = currentView()->selection();

	if (selectedList.count())
	{
		QFileInfo file(selectedList.value(0));
		if (file.isDir())
		{
			CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::Terminal, QFileInfo(selectedList.value(0)), "");
			return;
		}
		else
		{
			CPrime::MessageEngine::showMessage("org.cubocore.CoreFM", "CoreFM", "Can't open terminal here", "Please select a folder to open terminal there.");
			return;
		}
	}

	CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::Terminal, QFileInfo(currentView()->rootPath()), "");
}


void corefm::prepareMove()
{
	QStringList selectedItems = currentView()->selection();

	if (not selectedItems.count())
	{
		return;
	}

	moveItems = true;

	QList<QUrl> urlList;
	QByteArray  uriList;

	Q_FOREACH ( QString item, selectedItems )
	{
		QUrl url = QUrl::fromLocalFile(item);
		urlList << url;
		uriList += QString(url.toString() + "\n").toLocal8Bit();
	}

	QMimeData *mData = new QMimeData();

	mData->setUrls(urlList);

	/* GNOME, LXDE, XFCE: Taken from libfm-qt */
	mData->setData(QStringLiteral("x-special/gnome-copied-files"), QByteArrayLiteral("cut\n") + uriList);

	/* KDE: Taken from libfm-qt */
	mData->setData(QStringLiteral("text/uri-list"), uriList.replace("\n", "\r\n"));
	mData->setData(QStringLiteral("application/x-kde-cutselection"), QByteArrayLiteral("1"));

	clipBoard->setMimeData(mData);

	currentView()->clearSelection();
}


void corefm::prepareCopy()
{
	if (not currentView()->selection().count())
	{
		return;
	}

	moveItems = false;

	QList<QUrl> urlList;
	Q_FOREACH ( QString item, currentView()->selection())
	{
		urlList << QUrl::fromLocalFile(item);
	}

	QMimeData *mData = new QMimeData();
	mData->setUrls(urlList);

	clipBoard->setMimeData(mData);

	currentView()->clearSelection();
}


void corefm::paste()
{
	const QMimeData *mimeData      = clipBoard->mimeData();
	bool            shouldCutKde   = (mimeData->data("application/x-kde-cutselection").toInt() ? true : false);
	bool            shouldCutGnome = (mimeData->data("x-special/gnome-copied-files").startsWith("cut\n") ? true : false);

	if (mimeData->hasUrls())
	{
		CPrime::IOProcess *process = new CPrime::IOProcess();
		process->sourceDir = CPrime::FileUtils::dirName(mimeData->urls().at(0).toLocalFile());
		process->targetDir = currentView()->rootPath();

		if (not process->sourceDir.endsWith("/"))
		{
			process->sourceDir += "/";
		}

		QStringList srcList;
		Q_FOREACH ( QUrl url, mimeData->urls())
		{
			QString file = url.toLocalFile();
			if (CPrime::FileUtils::exists(file))
			{
				srcList << file.replace(process->sourceDir, "");
			}
		}

		if (not srcList.count())
		{
			return;
		}

		if (moveItems or shouldCutKde or shouldCutGnome)
		{
			process->type = CPrime::IOProcessType::Move;
		}

		else
		{
			process->type = CPrime::IOProcessType::Copy;
		}

		IODialog *pasteDlg = new IODialog(srcList, process);
		pasteDlg->show();
	}

	clipBoard->setMimeData(new QMimeData());
	pasteAct->setVisible(false);
	ui->paste->hide();
}


void corefm::refresh()
{
	if (view1->hasFocus())
	{
		view1->refresh();
	}

	else
	{
		view2->refresh();
	}
}


void corefm::selectAll()
{
	currentView()->selectAll();
}


void corefm::rename()
{
	filedialog dlg(filedialog::Rename, currentView()->rootPath(), currentView()->selection().value(0), this);

	dlg.exec();
}


void corefm::openRenamer()
{
	CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::BatchRenamer, QFileInfo(currentView()->selection().value(0)), "");
}


void corefm::openMetadataViewer()
{
	CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::MetadataViewer, QFileInfo(currentView()->selection().value(0)), "");
}


void corefm::trashSelected()
{
	CPrime::TrashManager::moveToTrash(currentView()->selection());
	ui->pageTrash->refresh();
}


void corefm::deleteSelected()
{
	CPrime::TrashManager::deleteFileTotally(currentView()->selection(), false);
}


void corefm::showProperties()
{
	QString currentFile;

	if (ui->stackedWidget->currentIndex() == 1)  // trash view
	{
		currentFile = CPrime::Variables::CC_Home_TrashDir() + "/files/";
	}
	else if (currentView()->selection().count())
	{
		currentFile = currentView()->selection().value(0);
	}
	else
	{
		currentFile = currentView()->rootPath();
	}

	propertiesw *props = new propertiesw(currentFile, this);

	props->show();
}


void corefm::goBack()
{
	if (ui->stackedWidget->currentIndex() == 1)
	{
		ui->stackedWidget->setCurrentIndex(0);
		ui->toolsBar->setEnabled(true);
	}

	currentView()->goBack();
}


void corefm::goForward()
{
	if (ui->stackedWidget->currentIndex() == 1)
	{
		ui->stackedWidget->setCurrentIndex(0);
		ui->toolsBar->setEnabled(true);
	}

	currentView()->goForward();
}


void corefm::goUp()
{
	if (ui->stackedWidget->currentIndex() == 1)
	{
		ui->stackedWidget->setCurrentIndex(0);
		ui->toolsBar->setEnabled(true);
	}

	currentView()->goUp();
}


void corefm::openSearch()
{
	QStringList selectedList = currentView()->selection();

	QString path = currentView()->rootPath();

	if (ui->stackedWidget->currentIndex() == 1)  // trash view
	{
		path = CPrime::Variables::CC_Home_TrashDir() + "/files/";
	}
	else     // files view
	{
		if (selectedList.count())
		{
			QFileInfo file(selectedList.value(0));
			if (file.isDir())
			{
				path = selectedList.value(0);
			}
			else
			{
				path = CPrime::FileUtils::dirName(selectedList.value(0));
			}
		}
	}

	qDebug() << "Opening search for " << path;

	CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::SearchApp, QFileInfo(path), "");
}


void corefm::writeTree()
{
	QString fileName = QFileDialog::getSaveFileName(
		this,
		"CoreFM - Save Directory Tree As",
		currentView()->rootPath(),
		"All Files ( *.* )"
		);

	if (not fileName.length())
	{
		return;
	}

	QString path;

	if (not currentView()->selection().count())
	{
		path = currentView()->rootPath();
	}

	else
	{
		path = currentView()->selection().value(0);
	}

	TreeWriter *writer = new TreeWriter(this);

	writer->writeTree(path, fileName);
}


void corefm::nextView()
{
	smi->setValue("CoreFM", "ViewMode", viewMode);

	/* IconView */
	if (viewMode == 0)
	{
		viewChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-grid-symbolic", "view-grid", "view-grid"));
	}

	/* TileView */
	else if (viewMode == 1)
	{
		viewChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-app-grid-symbolic", "view-list-details", "view-list-details"));
	}

	/* DetailsView */
	else
	{
		viewChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-list-symbolic", "view-list", "view-list"));
	}

	view1->refreshView(viewMode);
	view2->refreshView(viewMode);

	viewMode = (viewMode + 1) % 3;
}


void corefm::nextSortMode()
{
	smi->setValue("CoreFM", "SortMode", sortMode);

	/* Sort by Name */
	if (sortMode == 0)
	{
		sortChangeAct->setText("Name");
		sortChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"));
		view1->sort(0, Qt::AscendingOrder);
		view2->sort(0, Qt::AscendingOrder);
	}
	else if (sortMode == 1)
	{
		sortChangeAct->setText("Name");
		sortChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-sort-descending-symbolic", "view-sort-descending", "view-sort-descending"));
		view1->sort(0, Qt::DescendingOrder);
		view2->sort(0, Qt::DescendingOrder);
	}

	/* Sort by Size */
	else if (sortMode == 2)
	{
		sortChangeAct->setText("Size");
		sortChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"));
		view1->sort(1, Qt::AscendingOrder);
		view2->sort(1, Qt::AscendingOrder);
	}
	else if (sortMode == 3)
	{
		sortChangeAct->setText("Size");
		sortChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-sort-descending-symbolic", "view-sort-descending", "view-sort-descending"));
		view1->sort(1, Qt::DescendingOrder);
		view2->sort(1, Qt::DescendingOrder);
	}

	/* Sort by Time */
	else if (sortMode == 4)
	{
		sortChangeAct->setText("Time");
		sortChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-sort-ascending-symbolic", "view-sort-ascending", "view-sort-ascending"));
		view1->sort(3, Qt::AscendingOrder);
		view2->sort(3, Qt::AscendingOrder);
	}
	else if (sortMode == 5)
	{
		sortChangeAct->setText("Time");
		sortChangeAct->setIcon(CPrime::ThemeFunc::themeIcon("view-sort-descending-symbolic", "view-sort-descending", "view-sort-descending"));
		view1->sort(3, Qt::DescendingOrder);
		view2->sort(3, Qt::DescendingOrder);
	}

	sortMode = (sortMode + 1) % 6;
}


void corefm::updateVarious()
{
	ui->pathLbl->setText(QFileInfo(currentView()->rootPath()).fileName());
	setWindowTitle("CoreFM - " + CPrime::FileUtils::baseName(currentView()->rootPath()));

	ui->spaceLbl->setText(QString("%1 free").arg(CPrime::FileUtils::formatSize(QStorageInfo(currentView()->rootPath()).bytesAvailable())));

	QStringList selected = currentView()->selection();

	/* Some selection */
	if (selected.count())
	{
		cutAct->setVisible(true);
		ui->cut->setVisible(true);
		copyAct->setVisible(true);
		ui->copy->setVisible(true);
		trashAct->setVisible(true);
		ui->trashit->setVisible(true);
		selectAllAct->setVisible(false);
		ui->selectAll->setVisible(false);
		ui->newtext->setVisible(false);
		newFileAct->setVisible(false);
		ui->newfolder->setVisible(false);
		newFolderAct->setVisible(false);
		openWithAct->setVisible(true);
		ui->openWith->setVisible(true);

		/* Multiple nodes selected */
		if (selected.count() > 1)
		{
			quint64 folders = 0, files = 0;
			Q_FOREACH ( QString node, selected )
			{
				if (CPrime::FileUtils::isDir(node))
				{
					folders++;
				}

				else
				{
					files++;
				}
			}

			QString text = "";
			if (folders)
			{
				text += QString("%1 folder%2%3").arg(folders).arg(folders > 1 ? "s" : "").arg(files > 0 ? ", " : "");
			}

			if (files)
			{
				text += QString("%1 file%2").arg(files).arg(files > 1 ? "s" : "");
			}

			ui->sizeLbl->setText(text);

			ui->terminal->setVisible(false);
			termAct->setVisible(false);
			ui->search->setVisible(false);
			searchAct->setVisible(false);
			ui->rename->setVisible(false);
			renameAct->setVisible(false);
			ui->properties->setVisible(false);
			propsAct->setVisible(false);
		}

		/* Single node selected */
		else
		{
			quint64 count = 0;
			if (CPrime::FileUtils::isDir(selected.at(0)))
			{
				struct dirent **fileList;
				count = scandir(selected.at(0).toStdString().c_str(), &fileList, nullptr, nullptr) - 2;
				ui->sizeLbl->setText(QString("%1 item%2").arg(count).arg(count > 1 ? "s" : ""));
				ui->terminal->setVisible(true);
				termAct->setVisible(true);
				ui->search->setVisible(true);
				searchAct->setVisible(true);
			}

			else
			{
				count = CPrime::FileUtils::getFileSize(selected.at(0));
				ui->sizeLbl->setText(CPrime::FileUtils::formatSize(count));
				ui->terminal->setVisible(false);
				termAct->setVisible(false);
				ui->search->setVisible(false);
				searchAct->setVisible(false);
			}

			renameAct->setVisible(true);
			ui->rename->setVisible(true);
			propsAct->setVisible(true);
			ui->properties->setVisible(true);
		}
	}

	/* No selection */
	else
	{
		cutAct->setVisible(false);
		ui->cut->setVisible(false);
		copyAct->setVisible(false);
		ui->copy->setVisible(false);
		trashAct->setVisible(false);
		ui->trashit->setVisible(false);
		ui->newtext->setVisible(true);
		newFileAct->setVisible(true);
		ui->newfolder->setVisible(true);
		newFolderAct->setVisible(true);
		selectAllAct->setVisible(true);
		ui->selectAll->setVisible(true);
		ui->terminal->setVisible(true);
		termAct->setVisible(true);
		ui->search->setVisible(true);
		searchAct->setVisible(true);
		ui->rename->setVisible(false);
		renameAct->setVisible(false);
		ui->openWith->setVisible(false);
		openWithAct->setVisible(false);
		ui->properties->setVisible(true);
		propsAct->setVisible(true);

		/* The subtraction of 2 is to account for . and .. */
		struct dirent **fileList;
		quint64       count = scandir(currentView()->rootPath().toStdString().c_str(), &fileList, nullptr, nullptr) - 2;

		if (count)
		{
			ui->sizeLbl->setText(QString("%1 item%2").arg(count).arg(count > 1 ? "s" : ""));
		}

		else
		{
			ui->sizeLbl->setText("Empty");
		}
	}

	if (currentView()->canGoBack())
	{
		backAct->setEnabled(true);
	}
	else
	{
		backAct->setDisabled(true);
	}

	/* Clipboard */
	if (clipBoard->mimeData()->hasUrls())
	{
		pasteAct->setVisible(true);
		ui->paste->setVisible(true);
	}
	else
	{
		pasteAct->setVisible(false);
		ui->paste->setVisible(false);
	}

	/* Up action */
	if (currentView()->canGoUp())
	{
		upAct->setEnabled(true);
	}

	else
	{
		upAct->setDisabled(true);
	}
}


void corefm::openInApp(QString path)
{
	qDebug() << "func(openInApp-corefm) : Opening" << path << currentView()->selection();
	if (path.length())
	{
		CPrime::AppOpenFunc::appOpenEngine(path);
		return;
	}
	QAction *action = dynamic_cast<QAction *>(sender());

	if (action)
	{
		CPrime::DesktopFile app = action->data().value<CPrime::DesktopFile>();
		app.startApplicationWithArgs(currentView()->selection());
	}
}


void corefm::selectApp()
{
	// Select application in the dialog
	CPrime::ApplicationDialog *dialog = new CPrime::ApplicationDialog(listViewIconSize, this);


	if (dialog->exec())
	{
		if (dialog->getCurrentLauncher().compare("") != 0)
		{
			CPrime::AppOpenFunc::systemAppOpener(dialog->getCurrentLauncher(), currentView()->selection());

			// Function from utilities.cpp
			CPrime::ActivitiesManage::saveToActivites(dialog->getCurrentLauncher(), currentView()->selection());
		}
	}
}


void corefm::newWindow(QString path)
{
	corefm *cfm = new corefm(path);

	if (uiMode != 0)
	{
		cfm->showMaximized();
	}

	else
	{
		cfm->show();
	}
}


void corefm::openInSplitView()
{
	QString path;

	QAction *openAct = qobject_cast<QAction *>(sender());

	if (openAct)
	{
		path = currentView()->selection().value(0);
	}

	else
	{
		return;
	}

	view2->loadAddress(path);

	if (not view2->isVisible())
	{
		view2->show();
		view2->activateWindow();
		view2->setFocus();
	}
}


void corefm::loadAddress(QString path)
{
	QAction *openAct = qobject_cast<QAction *>(sender());

	ui->properties->show();
	ui->search->show();
	ui->pinIt->setEnabled(true);
	ui->shareIt->setEnabled(true);
	ui->tools->setEnabled(true);

	if (openAct)
	{
		currentView()->loadAddress(currentView()->selection().value(0));
	}
	else
	{
		// first hide the sideview to get more spaces
		if (uiMode == 2)
		{
			ui->sideView->setVisible(0);
		}

		if ((ui->stackedWidget->currentIndex() == 1) || (ui->stackedWidget->currentIndex() == 2))
		{
			ui->stackedWidget->setCurrentIndex(0);
		}

		if (ui->stackedWidget->currentIndex() == 0)
		{
			ui->toolsBar->setEnabled(true);
		}

		currentView()->loadAddress(path);
	}
}


void corefm::toggleSplit()
{
	if (view2->isVisible())
	{
		view2->hide();
		view1->activateWindow();
		view1->setFocus();
	}

	else
	{
		view2->show();
		view1->activateWindow();
		view1->setFocus();
	}
}


void corefm::on_shareIt_clicked()
{
	if (currentView()->selection().count())
	{
		ShareIT *t = new ShareIT(currentView()->selection(), listViewIconSize, nullptr);
		// Set ShareIT window size
		if (uiMode == 2)
		{
			t->setFixedSize(QGuiApplication::primaryScreen()->size() * .8);
		}
		else
		{
			t->resize(500, 600);
		}

		t->exec();
	}
	else
	{
		CPrime::MessageEngine::showMessage("org.cubocore.CoreFM", "CoreFM", "Warning!!!", "Nothing Selected.\nSelect some files to share.");
	}
}


void corefm::on_pinIt_clicked()
{
	QStringList selectedItems = currentView()->selection();

	if (selectedItems.count())
	{
		PinIT *pit = new PinIT(selectedItems, this);
		pit->exec();
	}

	else
	{
		PinIT *pit = new PinIT(QStringList() << currentView()->rootPath(), this);
		pit->exec();
	}
}


void corefm::on_home_clicked()
{
	loadAddress(QDir::homePath());
}


void corefm::on_desktop_clicked()
{
	loadAddress(QDir::home().filePath("Desktop"));
}


void corefm::on_downloads_clicked()
{
	loadAddress(QDir::home().filePath("Downloads"));
}


void corefm::on_root_clicked()
{
	loadAddress("/");
}


void corefm::on_trash_clicked()
{
	// first hide the sideview to get more spaces
	if (uiMode == 2)
	{
		ui->sideView->setVisible(0);
	}

	ui->stackedWidget->setCurrentIndex(1);
	ui->properties->show();
	ui->search->show();
	ui->toolsBar->hide();
	ui->copy->hide();
	ui->cut->hide();
	ui->paste->hide();
	ui->pinIt->setEnabled(false);
	ui->shareIt->setEnabled(false);
	ui->tools->setEnabled(false);

	int count = ui->pageTrash->model()->rowCount();
	ui->sizeLbl->setText(QString("%1 item%2").arg(count).arg(count > 1 ? "s" : ""));
}


void corefm::on_drives_clicked()
{
	// first hide the sideview to get more spaces
	if (uiMode == 2)
	{
		ui->sideView->setVisible(0);
	}

	ui->stackedWidget->setCurrentIndex(2);
	ui->toolsBar->hide();
	ui->properties->hide();
	ui->search->hide();
	ui->copy->hide();
	ui->cut->hide();
	ui->paste->hide();
	ui->pinIt->setEnabled(false);
	ui->shareIt->setEnabled(false);
	ui->tools->setEnabled(false);
	connect(sMgr, SIGNAL(deviceAdded(QString)), this, SLOT(loadDrivePage()));
	connect(sMgr, SIGNAL(deviceRemoved(QString)), this, SLOT(loadDrivePage()));

	loadDrivePage();
}


void corefm::loadDrivePage()
{
	ui->driveView->clear();

	Q_FOREACH (StorageDevice drive, sMgr->devices())
	{
		Q_FOREACH (StorageBlock block, drive.partitions())
		{
			if (block.fileSystem().isEmpty() and block.property("Partition", "Number").toInt() == 0)
			{
				continue;
			}

			if (block.property("Partition", "IsContainer").toBool())
			{
				continue;
			}

			QString name = block.label();

			if (name.isEmpty())
			{
				name = block.path().split("/").takeLast();
			}
			name = name.trimmed();

			QString         size           = CPrime::FileUtils::formatSize(block.totalSize());
			QListWidgetItem *itemPartition = new QListWidgetItem(ui->driveView);
			itemPartition->setIcon(QIcon::fromTheme("drive-multidisk"));
			itemPartition->setText(name + " (" + size + ")");
			itemPartition->setData(Qt::UserRole + 1, block.path());
			ui->driveView->addItem(itemPartition);
			ui->driveView->setIconSize(listViewIconSize);
		}
	}
	QString s = QString::number(ui->driveView->count());
	ui->sizeLbl->setText(s + " items ");
}


void corefm::openSelecteDrive(QListWidgetItem *item)
{
	QString blockPath = item->data(Qt::UserRole + 1).toString();

	if (blockPath.length())
	{
		QString      blockID = blockPath.split("/").takeLast();
		StorageBlock block(blockID);

		/* Try mounting anything that isn't swap */
		if (block.fileSystem() != "swap")
		{
			QString str = block.mountPoint().replace("\\040", " ");
			if (block.mount())
			{
				block = StorageBlock(blockID);
				str   = block.mountPoint().replace("\\040", " ");
				CPrime::MessageEngine::showMessage("org.cubocore.CoreFm", "CoreFM", "Mounted", "Partition '" + block.device() + "' mounted.");
			}
			ui->stackedWidget->setCurrentIndex(0);
			loadAddress(str);
		}
	}
}
