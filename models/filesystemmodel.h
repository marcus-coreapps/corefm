/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/


#pragma once

#include <QThread>
#include <QFileSystemModel>

#include "global.h"


class Thumbnailer : public QThread {
	Q_OBJECT

public:
	Thumbnailer(QObject *parent);

	/* We add files here, for which thumbnail generation is needed */
	void acquire(QString filename);

protected:
	/* Loop to generate thumbnails one by one and intimate the model */
	void run();

private:
	/* The actual thumbnail generation takes place here */
	bool getThumb(QString item);

	/* I don't trust Qthread::isRunning(), custom flag to indicate that */
	bool isActive;

	/* List of files whose thumbnails are to be generated. */
	QStringList fileList;

Q_SIGNALS:
	/* Signal to inform the model about thumbnail generated */
	void updateItem(QString);
};

class FileSystemModel : public QFileSystemModel {
	Q_OBJECT

public:
	FileSystemModel(QWidget *parent);
	~FileSystemModel();

	/* We want to ensure thumbnailing is possible for image files */
	QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/* Reload the model, applying name filters, dir filters, etc */
	void reload();

	/* Drag and Drop */
	Qt::DropActions supportedDropActions() const Q_DECL_OVERRIDE;
	Qt::DropActions supportedDragActions() const Q_DECL_OVERRIDE;
	Qt::ItemFlags flags(const QModelIndex& index) const Q_DECL_OVERRIDE;
	QStringList mimeTypes() const Q_DECL_OVERRIDE;

	/* When something is dragged */
	QMimeData * mimeData(const QModelIndexList& indexes) const Q_DECL_OVERRIDE;

	/* When something is dropped */
	bool dropMimeData(const QMimeData *, Qt::DropAction, int, int, const QModelIndex&) Q_DECL_OVERRIDE;

	/* Place to store the image thumbnails */
	static QHash<QString, QIcon> iconMap;

	/* Supported image formats */
	static QStringList supportedFormats;

private:
	/* Hook to obtain image thumbnails */
	Thumbnailer *thumbnailer;

	QModelIndex mCurIdx;
	settings *smi;

	static QFontMetrics fm;

public Q_SLOTS:
	/* Once a thumbnail is generated intimate the view about it */
	void updateItem(QString filename);

private Q_SLOTS:
	/* INTERNAL: currentIndex for font rendering */
	void setCurrentIndex(QModelIndex);

Q_SIGNALS:
	/* Signal to intimate the view about the index data change */
	void updateItem(QModelIndex);
};
