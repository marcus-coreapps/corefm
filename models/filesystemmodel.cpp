/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

// Local Headers
#include "filesystemmodel.h"

#include <QImageReader>
#include <QPainter>
#include <QWidget>
#include <QPalette>
#include <QMimeData>
#include <QUrl>
#include <QApplication>

#include <unistd.h>

#include <cprime/themefunc.h>
#include <cprime/filefunc.h>
#include <cprime/ioprocesses.h>

/* ##### Thumbnailer Class ========================================================================================================== */
Thumbnailer::Thumbnailer(QObject *parent) : QThread(parent)
{
	fileList.clear();
	isActive = false;
}


void Thumbnailer::acquire(QString filename)
{
	if (not fileList.contains(filename))
	{
		fileList << filename;
	}

	if (not isActive)
	{
		start();
	}
}


void Thumbnailer::run()
{
	while (true)
	{
		if (fileList.count())
		{
			isActive = true;

			QString filename = fileList.takeFirst();
			if (getThumb(filename))
			{
				emit updateItem(filename);
			}
		}

		else
		{
			isActive = false;
			return;
		}
	}
}


bool Thumbnailer::getThumb(QString item)
{
	// Thumbnail image
	QImage  pic;
	QPixmap thumb(QSize(128, 128));

	thumb.fill(Qt::transparent);

	QImageReader picReader(item);

	pic = picReader.read();
	if (pic.isNull())
	{
		qDebug() << "Bad image";
		return false;
	}

	pic = pic.scaled(512, 512, Qt::KeepAspectRatio, Qt::FastTransformation);
	pic = pic.scaled(128, 128, Qt::KeepAspectRatio, Qt::SmoothTransformation);

	QPainter painter(&thumb);

	painter.drawImage(QRectF(QPointF((128 - pic.width()) / 2, (128 - pic.height()) / 2), QSizeF(pic.size())), pic);
	painter.end();

	FileSystemModel::iconMap.insert(item, thumb);
	return true;
}


/* ##### File System Model ========================================================================================================== */

QStringList           FileSystemModel::supportedFormats = QStringList();
QHash<QString, QIcon> FileSystemModel::iconMap          = QHash<QString, QIcon>();

FileSystemModel::FileSystemModel(QWidget *parent) : QFileSystemModel(parent)
	, smi(new settings)
{
	Q_FOREACH ( QByteArray imgFmt, QImageReader::supportedImageFormats())
	{
		supportedFormats << QString::fromLatin1(imgFmt).toLower();
	}

	setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs);
	if (smi->getValue("CoreFM", "ShowHidden"))
	{
		setFilter(filter() | QDir::Hidden);
	}

	thumbnailer = new Thumbnailer(this);
	connect(thumbnailer, SIGNAL(updateItem(QString)), this, SLOT(updateItem(QString)));
}


FileSystemModel::~FileSystemModel()
{
	if (thumbnailer->isRunning())
	{
		thumbnailer->terminate();
	}
	thumbnailer->wait();

	delete thumbnailer;
	delete smi;
}


QVariant FileSystemModel::data(const QModelIndex& idx, int role) const
{
	switch (role)
	{
	case Qt::DecorationRole:
	   {
		   if (idx.column() != 0)
		   {
			   return QIcon();
		   }

		   QString fileName = idx.data().toString();
		   if (filePath(idx) == QDir::homePath())
		   {
			   return QIcon::fromTheme("user-home", QIcon::fromTheme("folder"));
		   }

		   if (isDir(idx) and (rootPath() == QDir::homePath()))
		   {
			   QIcon folder = QIcon::fromTheme("folder");
			   if (fileName.toLower() == "desktop")
			   {
				   return QIcon::fromTheme("user-desktop", folder);
			   }

			   else if (fileName.toLower() == "documents")
			   {
				   return QIcon::fromTheme("folder-documents", folder);
			   }

			   else if (fileName.toLower() == "downloads")
			   {
				   return QIcon::fromTheme("folder-downloads", folder);
			   }

			   else if (fileName.toLower() == "music")
			   {
				   return QIcon::fromTheme("folder-music", folder);
			   }

			   else if (fileName.toLower() == "pictures")
			   {
				   return QIcon::fromTheme("folder-pictures", folder);
			   }

			   else if (fileName.toLower() == "public")
			   {
				   return QIcon::fromTheme("folder-public", folder);
			   }

			   else if (fileName.toLower() == "templates")
			   {
				   return QIcon::fromTheme("folder-templates", folder);
			   }

			   else if (fileName.toLower() == "videos")
			   {
				   return QIcon::fromTheme("folder-videos", folder);
			   }

			   else
			   {
				   return folder;
			   }
		   }

		   /* If we can show thumbnails */
		   if (smi->getValue("CoreFM", "ShowThumb"))
		   {
			   if (isDir(idx))
			   {
				   return CPrime::ThemeFunc::getFileIcon(filePath(idx));
			   }

			   if (supportedFormats.contains(fileName.split(".").last().toLower()))
			   {
				   /* If thumbnail is not acquired, acquire it */
				   if (not iconMap.contains(rootPath() + "/" + fileName))
				   {
					   thumbnailer->acquire(rootPath() + "/" + fileName);
				   }

				   /* Acquired thumbnail */
				   else
				   {
					   return iconMap.value(rootPath() + "/" + fileName);
				   }
			   }
		   }

		   return CPrime::ThemeFunc::getFileIcon(filePath(idx));
	   }

	case Qt::ForegroundRole:
	   {
		   QFileInfo type(fileInfo(idx));
		   QPalette  pltt = qApp->palette();

		   if (type.isSymLink())
		   {
			   return pltt.link();
		   }

		   if (type.isDir())
		   {
			   return pltt.windowText();
		   }

		   if (not type.isReadable())
		   {
			   return QBrush(Qt::darkRed);
		   }

		   if (type.isExecutable())
		   {
			   return QBrush(Qt::darkGreen);
		   }

		   return QFileSystemModel::data(idx, role);
	   }

	case Qt::FontRole:
	   {
		   QFileInfo type(fileInfo(idx));
		   QFont     font(QFileSystemModel::data(idx, role).value<QFont>());

		   if (type.isSymLink())
		   {
			   return QFont(font.family(), font.pointSize(), font.weight(), true);
		   }

		   return font;
	   }

	default:

		return QFileSystemModel::data(idx, role);
	}
}


void FileSystemModel::reload()
{
	setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs);
	if (smi->getValue("CoreFM", "ShowHidden"))
	{
		setFilter(filter() | QDir::Hidden);
	}

	setRootPath(rootPath());
}


Qt::DropActions FileSystemModel::supportedDragActions() const
{
	return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}


Qt::DropActions FileSystemModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}


Qt::ItemFlags FileSystemModel::flags(const QModelIndex& idx) const
{
	Qt::ItemFlags itemFlags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	QFileInfo     finfo     = fileInfo(idx);

	if (finfo.isReadable())
	{
		if (finfo.isDir() and finfo.isExecutable())
		{
			itemFlags |= Qt::ItemIsDragEnabled;
		}

		else if (finfo.isFile())
		{
			itemFlags |= Qt::ItemIsDragEnabled;
		}
	}

	if (finfo.isDir() and finfo.isWritable() and finfo.isExecutable())
	{
		itemFlags |= Qt::ItemIsDropEnabled;
	}

	return itemFlags;
}


QStringList FileSystemModel::mimeTypes() const
{
	QStringList types;

	types << "text/uri-list";
	return types;
}


QMimeData * FileSystemModel::mimeData(const QModelIndexList& indexes) const
{
	QMimeData *data = new QMimeData();

	QList<QUrl> files;

	Q_FOREACH ( QModelIndex index, indexes )
	{
		files << QUrl::fromLocalFile(filePath(index));
	}

	data->setUrls(files);

	return data;
}


bool FileSystemModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int, int, const QModelIndex& parent)
{
	if (not data->hasUrls())
	{
		qDebug() << data;
		return false;
	}

	if (not parent.isValid())
	{
		qDebug() << filePath(parent);
		return false;
	}

	if (action == Qt::IgnoreAction)
	{
		qDebug() << "Ignoring drop";
		return false;
	}

	QList<QUrl> urls = data->urls();
	QStringList srcList;

	QString           target    = filePath(parent);
	CPrime::IOProcess *progress = new CPrime::IOProcess;

	/* No source dir. All the urls will be absolute */
	progress->sourceDir  = CPrime::FileUtils::dirName(urls.value(0).toLocalFile());
	progress->sourceDir += (progress->sourceDir.endsWith("/") ? "" : "/");
	Q_FOREACH ( QUrl src, urls )
	{
		srcList << src.toLocalFile().replace(progress->sourceDir, "");
	}

	progress->targetDir = filePath(parent);

	switch (action)
	{
	case Qt::CopyAction:

		progress->type = CPrime::Copy;
		break;

	case Qt::MoveAction:

		progress->type = CPrime::Move;
		break;

	case Qt::LinkAction:

		Q_FOREACH ( QString source, srcList )
		{
			symlink(source.toLocal8Bit().constData(), rootDirectory().filePath(CPrime::FileUtils::baseName(source)).toLocal8Bit().constData());
		}

		return true;

	default:
		return false;
	}

	IODialog *pasteDlg = new IODialog(srcList, progress);
	pasteDlg->show();

	return true;
}


void FileSystemModel::updateItem(QString filename)
{
	QModelIndex idx = QFileSystemModel::index(filename);
	emit        updateItem(idx);
}


void FileSystemModel::setCurrentIndex(QModelIndex idx)
{
	mCurIdx = idx;
}
