/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/


#include "trashmodel.h"

#include <QStorageInfo>

#include <cprime/filefunc.h>
#include <cprime/trashmanager.h>


CTrashNode::CTrashNode(const QString& path, const QString& trashPath, const QString& dateTime)
{
	mName = CPrime::FileUtils::baseName(path);
	mPath = QString(path);

	mIcon      = QIcon::fromTheme(mimeDbInstance.mimeTypeForFile(trashPath).iconName());
	mTrashPath = QString(trashPath);

	mDate = QDateTime::fromString(dateTime, "yyyyMMddTHH:mm:ss");
	if (mDate.isNull())
	{
		mDate = QDateTime::fromString(dateTime, "yyyy-MM-ddTHH:mm:ss");
	}
}


QVariant CTrashNode::data(int role, int column)
{
	switch (role)
	{
	case Qt::DisplayRole:

		if (column == 0)
		{
			return mName;
		}

		else if (column == 1)
		{
			return mDate;
		}

		else if (column == 2)
		{
			return mPath;
		}

		else
		{
			return QVariant();
		}

	case Qt::DecorationRole:

		return mIcon;

	case Qt::UserRole + 1:

		return mTrashPath;

	default:
		return QVariant();
	}
}


CTrashModel::CTrashModel() : QAbstractItemModel()
{
	setupModelData();
}


CTrashModel::~CTrashModel()
{
	nodes.clear();
}


int CTrashModel::rowCount(const QModelIndex&) const
{
	return nodes.count();
}


int CTrashModel::columnCount(const QModelIndex&) const
{
	return 3;
}


Qt::ItemFlags CTrashModel::flags(const QModelIndex& index) const
{
	if (not index.isValid())
	{
		return Qt::NoItemFlags;
	}

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}


QVariant CTrashModel::data(const QModelIndex& index, int role) const
{
	if (not index.isValid())
	{
		return QVariant();
	}

	CTrashNode *node = static_cast<CTrashNode *>(index.internalPointer());
	switch (role)
	{
	case Qt::DisplayRole:
		switch (index.column())
		{
		case 0:
			return node->data(role, 0);

		case 1:
			return node->data(role, 1).value<QDateTime>().toString("MMM dd, yyyy hh:mm:ss");

		case 2:
			return node->data(role, 2);
		}

		return QString();

	case Qt::DecorationRole:
		if (index.column() == 0)
		{
			return node->data(role, 0);
		}

		else
		{
			return QVariant();
		}

	case Qt::TextAlignmentRole:
		if (index.column() == 0)
		{
			return(0x0001 | 0x0080);
		}

		else if (index.column() == 2)
		{
			return(0x0001 | 0x0080);
		}

		else
		{
			return Qt::AlignCenter;
		}

	case Qt::InitialSortOrderRole:

		return Qt::AscendingOrder;

	case Qt::UserRole + 1:

		return node->data(Qt::UserRole + 1, 0);

	default:

		return QVariant();
	}
}


QVariant CTrashModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QStringList headerList = QStringList() << "Name" << "Deleteion Date" << "Original Path";

	if ((orientation == Qt::Horizontal) and (role == Qt::DisplayRole))
	{
		return headerList.at(section);
	}

	else
	{
		return QVariant();
	}
}


QModelIndex CTrashModel::index(int row, int column, const QModelIndex&) const
{
	if ((row >= 0) and (row < nodes.count()))
	{
		return createIndex(row, column, nodes.at(row));
	}

	else
	{
		return QModelIndex();
	}
}


QModelIndex CTrashModel::parent(const QModelIndex&) const
{
	return QModelIndex();
}


void CTrashModel::reload()
{
	setupModelData();
}


void CTrashModel::setupModelData()
{
	nodes.clear();

	beginResetModel();

	/* Loading home trash */
	QString trashLoc = CPrime::Variables::CC_Home_TrashDir();
	QDir    trashDir = QDir(trashLoc + "/files/");
	trashDir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
	Q_FOREACH ( QFileInfo info, trashDir.entryInfoList())
	{
		QString trashPath = info.filePath();
		QString infoPath  = trashLoc + "/info/" + info.fileName() + ".trashinfo";

		QSettings trashInfo(infoPath, QSettings::NativeFormat);
		QString   origPath = trashInfo.value("Trash Info/Path").toString();
		QString   dateTime = trashInfo.value("Trash Info/DeletionDate").toString();

		nodes << new CTrashNode(origPath, trashPath, dateTime);
	}

	/* Loading all trash */
	Q_FOREACH ( QStorageInfo devInfo, QStorageInfo::mountedVolumes())
	{
		/* We don't want to check the home directory for trash once more */
		if (devInfo.rootPath() == QDir::homePath())
		{
			continue;
		}

		QString trashLoc = CPrime::TrashManager::trashLocation(devInfo.rootPath());
		if (CPrime::Variables::CC_Home_TrashDir() == trashLoc)
		{
			continue;
		}

		if (not trashLoc.isEmpty())
		{
			QDir trash(trashLoc + "/info/");
			trash.setNameFilters(QStringList() << "*.trashinfo");
			Q_FOREACH ( QString entry, trash.entryList())
			{
				QSettings trashInfo(trash.absoluteFilePath(entry), QSettings::NativeFormat);

				QString origPath  = trashInfo.value("Trash Info/Path").toString();
				QString delDate   = trashInfo.value("Trash Info/DeletionDate").toString();
				QString trashPath = QString(trashInfo.fileName()).replace("/info/", "/files/").replace(".trashinfo", "");

				nodes << new CTrashNode(origPath, trashPath, delDate);
			}
		}
	}

	endResetModel();
}
