/**
  * This file is a part of CoreFM.
  * A file manager for C Suite.
  * * Copyright 2019-2023 CuboCore Group
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  * MA 02110-1301, USA.
  **/

#include <QApplication>
#include <QMessageBox>
#include <QDir>
#include <QDebug>

#include <unistd.h>

#include "corefm.h"

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	// Set application info
	app.setOrganizationName("CuboCore");
	app.setApplicationName("CoreFM");
	app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
	app.setDesktopFileName("org.cubocore.CoreFM");
	app.setQuitOnLastWindowClosed(true);

	/* Disable root access */
	if (not geteuid())
	{
		QMessageBox::critical(
			nullptr,
			"CoreFM - Root access disabled",
			"You are trying to run CoreFM as root. This is potentially dangerous, if you accidentally delete system files."
			"To avoid such damage to system files, CoreFM will now close."
			);

		return 256;
	}

	QString path = QDir::homePath();

	if (argc >= 2)
	{
		if (QDir(argv[1]).exists())
		{
			path = argv[1];
		}
		else
		{
			qWarning() << "Invalid filepath provided" << argv[1];
		}
	}

	corefm fm(path);

	fm.show();

	return app.exec();
}
